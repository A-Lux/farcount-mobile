import 'package:farcount/views/Sales/CreateInvoice.dart';
import 'package:flutter/material.dart';

import 'Setting.dart';

class AluxAppBar extends StatelessWidget implements PreferredSizeWidget {
  final PreferredSizeWidget bottom;

  AluxAppBar({ Key key, this.bottom }) : super(key: key);

  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
        title: Padding(
          padding: const EdgeInsets.only(top:22),
          child: Image.asset('assets/appbar.png',width: 72,),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(
            color: Colors.black,
            size: 50
        ),
        elevation: 1,
      leading: Padding(
        padding: const EdgeInsets.only(top: 12,bottom: 20),
        child: IconButton(icon: Icon(Icons.menu,size: 50,), onPressed: () {
          audioPlay();

          Scaffold.of(context).openDrawer();
        },),

      ),
      actions: [
        GestureDetector(
          onTap: (){
            audioPlay();

            Navigator.push(context, MaterialPageRoute(builder: (context)=>InvoiceCreatePage(isSaleInvoice: true,)));
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 12,right: 16,bottom: 20),
            child: Icon(Icons.add_circle,color: Theme.of(context).primaryColor,size: 60,),
          ),
        ),
      ],
      toolbarHeight: 75,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(75.0);
}