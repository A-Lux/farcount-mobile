
import 'package:farcount/AppViewController.dart';
import 'package:farcount/LOGINPAGES/Authorization.dart';
import 'package:farcount/LOGINPAGES/RegistrationScreen.dart';
import 'package:farcount/LOGINPAGES/WelcomeScreen.dart';
import 'package:farcount/Pages/Sells.dart';
import 'package:farcount/views/Analytics/Debtors.dart';
import 'package:farcount/views/Analytics/Lenders.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:farcount/views/Analytics/TopSales.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:farcount/views/Sales/CreateInvoice.dart';
import 'package:farcount/views/Sales/History.dart';
import 'package:farcount/views/SellAnalyticView.dart';
import 'package:farcount/views/Stack/HistoryStack.dart';
import 'package:farcount/views/Stack/StockBalance.dart';
import 'package:farcount/views/Stack/StockList.dart';
import 'package:farcount/views/answers/Success.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return GestureDetector(
      onTap: (){

      },
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('ru', 'RU'),
        ],
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Color(0xff1FBCFF),
          secondaryHeaderColor: Color(0xffDFE0E3),
          accentColor: Color(0xffC2CACD),
          appBarTheme: AppBarTheme(
            color: Colors.white
          ),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: WelcomeScreen(),
        routes: {
          '/dashboard': (context)=>AppViewController(),
          '/sales': (context)=>SellsPage(),
          AuthorizationScreen.id:(context)=>AuthorizationScreen(),
          RegistrationScreen.id:(context)=>RegistrationScreen(),
          AppViewController.id:(context)=>AppViewController(),
          OweList.id:(context)=>OweList(),
          OwePersonView.id:(context)=>OwePersonView(),
          SaleAnalyticScene.id:(context)=>SaleAnalyticScene(),
          HistoryPage.id:(context)=> HistoryPage(),
          InvoiceCreatePage.id :(context)=>InvoiceCreatePage(),
          SuccessPage.id: (context)=>SuccessPage(),
          SellsAnalyticPage.id:(context) =>SellsAnalyticPage(),
          StockBalancePage.id:(context) => StockBalancePage(),
          DebtorsPage.id: (context)=>DebtorsPage(),
          LendersPage.id: (context)=>LendersPage(),
          TopSalesPage.id: (context)=>TopSalesPage(),
          LendersList.id: (context)=>LendersList(),
          HistoryStockPage.id: (context)=>HistoryStockPage(),
        },
      ),
    );
  }
}