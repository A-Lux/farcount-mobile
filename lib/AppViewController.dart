import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:farcount/LOGINPAGES/Authorization.dart';
import 'package:farcount/LOGINPAGES/RegistrationScreen.dart';
import 'package:farcount/LOGINPAGES/WelcomeScreen.dart';
import 'package:farcount/Pages/Analyrics.dart';
import 'package:farcount/Pages/MainPage.dart';
import 'package:farcount/Pages/Profile.dart';
import 'package:farcount/Pages/Sclad.dart';
import 'package:farcount/Pages/Sells.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/bottomNabBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'Setting.dart';

class AppViewController extends StatefulWidget {
  static var id = 'appviewc';
  @override
  State<StatefulWidget> createState() {
    return _AppViewControllerState();
  }
}

class _AppViewControllerState extends State<AppViewController> {

    List<Widget> _buildScreens(){
      return [
        MainPage(),
        ProfilePage(),
        SellsPage(),
        ScladPage(),
        Analyticspage(),
      ];
    }


  List<PersistentBottomNavBarItem> _navBarsItem(){
    return [
      PersistentBottomNavBarItem(
          icon: Icon(Icons.home),
          title: ('Главное'),
          activeColorPrimary: Theme.of(context).primaryColor,
          inactiveColorPrimary: Theme.of(context).accentColor,


      ),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.account_box),
          title: ('Профиль'),
          activeColorPrimary: Theme.of(context).primaryColor,
          inactiveColorPrimary: Theme.of(context).accentColor,


      ),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.storage),
          title: ('Продажи'),
          activeColorPrimary: Theme.of(context).primaryColor,
          inactiveColorPrimary: Theme.of(context).accentColor,


      ),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.add_shopping_cart),
          title: ('Склад'),
          activeColorPrimary: Theme.of(context).primaryColor,
          inactiveColorPrimary: Theme.of(context).accentColor


      ),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.analytics),
          title: ('Аналитика'),
          activeColorPrimary: Theme.of(context).primaryColor,
          inactiveColorPrimary: Theme.of(context).accentColor,

      ),
    ];
  }


  int _viewIndex;
  Map<String,dynamic> user;
  Future getRedirect()async{
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    try{
      final res = await http.post('$endpointApi/user',headers: {
        "Authorization" : "Bearer $token"
      });
      final jsonResp = jsonDecode(res.body);
      prefs.setString('userData', jsonEncode(jsonResp));


    }catch(e){
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => WelcomeScreen()),
            (Route<dynamic> route) => false,
      );
    }


  }

  final bucket = PageStorageBucket();
  PersistentTabController _controller;
    AudioCache audioCache = AudioCache();

  @override
  void initState() {
    _viewIndex = 0;
    _controller = PersistentTabController(initialIndex: 0);

    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return  PersistentTabView(
          context,
          controller: _controller,
          screens: _buildScreens(),
          items: _navBarsItem(),
          backgroundColor: Colors.white,
          handleAndroidBackButtonPress: true,
          resizeToAvoidBottomInset: true,
          onItemSelected: (int) {

            setState(() {}); // This is required to update the nav bar if Android back button is pressed
          },
          stateManagement: true,
          hideNavigationBarWhenKeyboardShows: true,
          decoration: NavBarDecoration(
            borderRadius: BorderRadius.circular(0)
          ),
          popAllScreensOnTapOfSelectedTab: true,
          popActionScreens: PopActionScreensType.all,
          navBarStyle: NavBarStyle.style6,



    );
  }

  void _changeView(int index) {
    setState(() {
      _viewIndex = index;
    });
  }

}