import 'dart:convert';
import 'dart:math';

import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/Analytics/TopSales.dart';
import 'package:farcount/views/HelpWebView.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:shared_preferences/shared_preferences.dart';

import '../Setting.dart';
class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<dynamic> debtors = [];
  Future<dynamic> getDebtors()async{
    final prefs = await SharedPreferences.getInstance();


    final token = prefs.getString('token');
    var res = await http.get('${endpointApi}/waybills?sort_type=debt&sort_value=desc',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"
    },);
    var w = MediaQuery.of(context).size.width;
    print(res.body.toString());
    var responseBody = jsonDecode(res.body)['data'];
    print(responseBody);
    setState(() {

      debtors = responseBody;
      var i = 1;


    });

    return 'success';
  }
  @override
  void initState() {
    print('wdwd');
    // TODO: implement initState
    super.initState();
    this.getDebtors();
  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return AppPage(
        content: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16,top: 24),
                  child: Text('Создать накладную просто',style: TextStyle(
                      color: Colors.black,
                      fontSize: 28
                  ),),
                ),
                SizedBox(height: 12,),
                Padding(
                  padding: const EdgeInsets.only(right: 16,bottom: 24,left: 16),
                  child: Text('Создавайте накладные в 2 клика и сохраняйте в удлобном формате',style: TextStyle(
                      color: Color(0xffC2CACD),
                      fontSize: 16
                  ),),
                ),
                Container(
                  width: w,
                  height: 1,
                  color: Color(0xffDFE0E3),
                ),
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 16,right: 16,top: 16,bottom: 16),
                            child: GestureDetector(
                                onTap: (){
                                  audioPlay();

                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>HelpPage(title: 'Инструкция',url: '/help',)));
                                },
                                child: DonatInstruc(icon: Icons.content_paste,text: 'Инструкция',)
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(bottom: 16),
                            child: Container(width: w,height: 110,
                            color: Colors.white,
                            child: Center(child: Text(
                              'Рекламный баннер',style: TextStyle(
                              color: Color(0xffC2CACD),
                              fontSize: 18
                            ),
                            ),),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 16),
                            child: Container(
                              width: w,
                              height: 1,
                              color: Color(0xffDFE0E3),
                            ),
                          ),


                          SizedBox(
                            height: 50,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
    );
  }
}
class DonatInstruc extends StatelessWidget {
  final icon;
  final text;
  DonatInstruc({this.text,this.icon});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color:  Color.fromRGBO(117, 120, 123, 0.15,),
              spreadRadius: 4,
              blurRadius: 8,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ]
      ),
      width: w,
      height: 65,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            left: 22,
            child: Icon(icon,color: Theme.of(context).primaryColor,size: 40,),
          ),
          Text(text,style: TextStyle(
            fontSize: 18,
          ),)
        ],
      ),
    );
  }
}
class PersonCard extends StatelessWidget {
  String personName;
  int price;
  int id;
  int paid;
  PersonCard({Key key ,this.personName,this.price,this.id ,this.paid}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>OwePersonView(waybill: this.id,)));
      },
      child: Container(
        width: w/2-22,
        height: 90,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: Color(0xffEBF9FF)
        ),
        child: Stack(
          children: [
            Positioned(
              top: 12,
              left: 14,
              right: 32,
              child: Text(
                '${personName}',
                style: TextStyle(
                  fontSize: w*0.04,
                ),
              ),
            ),
            Positioned(
              bottom: 12,
              left: 14,
              right: 32,
              child: Text(
                '${price-paid} ₸',
                style: TextStyle(
                  color: Color(0xffBECED6),
                  fontSize: w*0.04,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                width: w/7,
                  height: 90,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomRight: Radius.circular(14)),
                      image: DecorationImage(image: AssetImage('assets/people.png',),alignment: Alignment.bottomRight),
                  ),
            ),
            ),
          ],
        ),
      ),
    );
  }
}
