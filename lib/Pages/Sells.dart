
import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/Sales/CreateInvoice.dart';
import 'package:farcount/views/Sales/History.dart';
import 'package:flutter/material.dart';

import '../Setting.dart';
class SellsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return AppPage(
      content: Stack(
        alignment: Alignment.center,
        children: [
          Heading('Продажи'),
          Container(
            width: w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    audioPlay();

                    Navigator.push(context, MaterialPageRoute(builder: (context)=>InvoiceCreatePage(isSaleInvoice: true,)));
                  },
                  child: Container(
                    width: w-32,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Theme.of(context).primaryColor,
                      boxShadow: [
                        BoxShadow(
                          color:  Theme.of(context).primaryColor.withOpacity(0.5),
                        ),
                      ]
                    ),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Text('Создать накладную продажи',style: TextStyle(
                            fontSize: 18,
                            color: Colors.white
                        ),),

                      ],
                    ),
                  ),
                ),
                SizedBox(height: 8,),
                GestureDetector(
                  onTap: (){
                    audioPlay();

                    Navigator.push(context, MaterialPageRoute(builder: (context)=>OweList()));
                  },
                  child: Container(
                    width: w-32,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Theme.of(context).primaryColor
                    ),
                    child: Center(
                      child: Text('Список должников',style: TextStyle(
                          fontSize: 18,
                          color: Colors.white
                      ),),
                    ),
                  ),
                ),
                SizedBox(height: 8,),
                GestureDetector(
                  onTap: (){
                    audioPlay();

                    Navigator.push(context, MaterialPageRoute(builder: (context)=>HistoryPage()));
                  },
                  child: Container(
                    width: w-32,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Theme.of(context).primaryColor,
                        border: Border.all(color: Theme.of(context).primaryColor)
                    ),
                    child: Center(
                      child: Text('Смотреть историю',style: TextStyle(
                          fontSize: 18,
                          color: Colors.white
                      ),),
                    ),
                  ),
                ),

              ],
            ),
          )
        ],
      )
    );
  }
}
