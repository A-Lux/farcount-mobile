import 'dart:convert';
import 'dart:math';

import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/Sales/CreateInvoice.dart';
import 'package:farcount/views/Stack/HistoryStack.dart';
import 'package:farcount/views/Stack/StockBalance.dart';
import 'package:farcount/views/Stack/StockList.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../Setting.dart';

class ScladPage extends StatefulWidget {
  @override
  _ScladPageState createState() => _ScladPageState();
}

class _ScladPageState extends State<ScladPage> {

  Map<String,dynamic> userData;


  Future<dynamic> getUser()async{


    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final res = await http.get('$endpointApi/user',headers: {
      "Authorization" : "Bearer $token"
    });

    setState(() {
      userData = jsonDecode(res.body);
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getUser();

  }

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return AppPage(
        content: Stack(
          alignment: Alignment.center,
          children: [
            Heading('Склад'),
            Container(
              width: w,
              child:userData != null ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  GestureDetector(
                    onTap: (){
                      audioPlay();

                      Navigator.push(context, MaterialPageRoute(builder: (context)=>InvoiceCreatePage(isSaleInvoice: false,)));

                    },
                    child: Container(
                      width: w-32,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Theme.of(context).primaryColor,
                          boxShadow: [
                            BoxShadow(
                              color:  Theme.of(context).primaryColor.withOpacity(0.5),
                              // changes position of shadow
                            ),
                          ]
                      ),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [

                          Text('Создать накладную поступления',style: TextStyle(
                              fontSize: 18,
                              color: Colors.white
                          ),),

                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 8,),
                  userData['role_id'] == 1?
                  GestureDetector(
                    onTap: (){
                      audioPlay();

                      Navigator.push(context, MaterialPageRoute(builder: (context)=>StockBalancePage()));
                    },
                    child: Container(
                      width: w-32,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Theme.of(context).primaryColor),
                          color: Theme.of(context).primaryColor
                      ),
                      child: Center(
                        child: Text('Остатки на складе',style: TextStyle(
                            fontSize: 18,
                            color: Colors.white
                        ),),
                      ),
                    ),
                  ) : SizedBox.shrink(),
                  SizedBox(height: 8,),

                  GestureDetector(
                    onTap: (){
                      audioPlay();

                      Navigator.push(context, MaterialPageRoute(builder: (context)=>OweList(debtProp: 1,)));
                    },
                    child: Container(
                      width: w-32,
                      height: 70,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Center(
                        child: Text('Список кредиторов',style: TextStyle(
                            fontSize: 18,
                            color: Colors.white
                        ),),
                      ),
                    ),
                  ),
                  SizedBox(height: 8,),
                  GestureDetector(
                    onTap:(){
                      audioPlay();

                      Navigator.push(context, MaterialPageRoute(builder: (context)=>HistoryStockPage()));
                    },
                    child: Container(
                      width: w-32,
                      height: 70,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Theme.of(context).primaryColor)
                      ),
                      child: Center(
                        child: Text('Смотреть историю',style: TextStyle(
                            fontSize: 18,
                            color: Colors.white
                        ),),
                      ),
                    ),
                  ),

                ],
              ):
              Center(
                child: CircularProgressIndicator(),
              ),
            )
          ],
        )
    );
  }
}
