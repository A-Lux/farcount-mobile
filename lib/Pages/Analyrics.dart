import 'dart:convert';

import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/Analytics/Debtors.dart';
import 'package:farcount/views/Analytics/Lenders.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:farcount/views/Analytics/TopSales.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/SellAnalyticView.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:shared_preferences/shared_preferences.dart';

import '../Setting.dart';
class Analyticspage extends StatefulWidget {
  @override
  _AnalyticspageState createState() => _AnalyticspageState();
}

class _AnalyticspageState extends State<Analyticspage> {
  Map<String,dynamic> userData;

  List<dynamic> sellers;
  Future<dynamic> getUser()async{


    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final res = await http.get('$endpointApi/user',headers: {
      "Authorization" : "Bearer $token"
    });

    setState(() {
      userData = jsonDecode(res.body);
    });

  }

  Future<dynamic> getSellers() async{
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final res = await http.get('$endpointApi/sellers?shop_id=${userData['shop_id']}',headers: {
      "Authorization" : "Bearer $token"
    });
    final responseJson = jsonDecode(res.body);
    setState(() {
      sellers = responseJson['users'];
    });
    print(sellers);
    return 'success';
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.getUser();

  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return AppPage(
        content: Stack(
          alignment: Alignment.center,
          children: [
            Heading('Аналитика'),
            Container(
              width: w,
              child: userData == null ? Center(child: CircularProgressIndicator(),) : userData['role_id'] == 1 ? Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  BlueTextButton(text: 'Топ должников',onTap: (){
                    audioPlay();

                    Navigator.push(context, MaterialPageRoute(builder: (context)=>DebtorsPage()));

                  },),
                  SizedBox(height: 20,),
                  BlueTextButton(text: 'Топ кредиторов',onTap: (){
                    audioPlay();

                    Navigator.push(context, MaterialPageRoute(builder: (context)=>LendersPage()));

                  },),
                  SizedBox(height: 20,),
                  BlueTextButton(text: 'Анализ продаж',onTap: (){
                    audioPlay();

                    Navigator.push(context, MaterialPageRoute(builder: (context)=>SellsAnalyticPage()));},),

                  SizedBox(height: 20,),
                  BlueTextButton(text: 'Топ продаж',onTap: (){
                    audioPlay();
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>TopSalesPage()));
                  },),
                ],
              ) : Center(child: Text('У вас нед доступа к аналитике'),),

            )
          ],
        )
    );
  }
}

class BlueTextButton extends StatelessWidget {
  final text;
  final onTap;
  BlueTextButton({this.text,this.onTap});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: this.onTap,
      child: Container(

        width: w-32,
        height: 70,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Theme.of(context).primaryColor,
            boxShadow: [
              BoxShadow(
                color:  Theme.of(context).primaryColor.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 8,
                offset: Offset(0, 4), // changes position of shadow
              ),
            ]
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text(text,style: TextStyle(
                fontSize: 18,
                color: Colors.white
            ),),

          ],
        ),
      ),
    );
  }
}
