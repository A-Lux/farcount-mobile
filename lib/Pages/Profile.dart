import 'dart:convert';

import 'package:farcount/Widgets/CustomTextField.dart';
import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:sweetalert/sweetalert.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../Setting.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _newUserNameController = TextEditingController();
  TextEditingController _newUserPhoneController = TextEditingController();
  TextEditingController _newUserPasswordController = TextEditingController();
  var maskFormatter = new MaskTextInputFormatter(mask: '+# (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });
  var maskFormatterNewUser = new MaskTextInputFormatter(mask: '+# (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });

  Map<String,dynamic> userData;
  bool registerUser = false;
  bool loadingRegister = false;
  List<dynamic> sellers = [];
  bool hideSellers = true;
  bool loadingUpdateUser = false;
  TextEditingController _passwordController = TextEditingController();
  Future<dynamic> deleteUser(userId) async{
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    
    final res = await http.get('$endpointApi/sanctum/destroy/$userId');
    if(res.statusCode == 200){
      SweetAlert.show(context,
          title: 'Успешно ',
          subtitle: 'Профиль удален',
          style: SweetAlertStyle.success
      );
    }
    this.getUser();
    hideSellers = true;
  }
  Future<dynamic> updateUser() async{
    List<String> errors = [];
    if(_phoneController.text == ''){

      errors.add('Введите номер ');

    }
    if(_emailController.text == ''){
      errors.add('Введите email ');
    }

    if(_nameController.text == ''){
      errors.add('Введите имя ');
    }


    if(_passwordController.text != '' && _nameController.text != '' && _phoneController.text != ''  && _emailController.text != ''){
      if(_passwordController.text.length < 8){
        errors.add('Пароль должен быть больше 8 символов');
      }
    }


    if(errors.length >0){
      SweetAlert.show(
        context,
        title: 'Ошибка',
        subtitle: errors.join('\n'),
        style: SweetAlertStyle.error,
      );
      return false;

    }

    setState(() {
      loadingUpdateUser = true;

    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print(token);
    if(_passwordController.text != ''){
      final res = await http.post('$endpointApi/sanctum/update',
          headers: {
            "Authorization" : "Bearer $token",
            "Accept" : "application/json"
          },
          body:{
            "user_id" :  userData['id'].toString(),
            "first_name" : _nameController.text,
            "phone" : _phoneController.text,
            "email" : _emailController.text,
            "password" : _passwordController.text,
          }

      );
      _passwordController.text = '';
      final resBody = jsonDecode(res.body);
      print(res.body);
      loadingUpdateUser = false;
      if(res.statusCode == 200){
        setState(() {

          userData = resBody['user'];

        });
      }
    }else{
      final res = await http.post('$endpointApi/sanctum/update',
          headers: {
            "Authorization" : "Bearer $token",
            "Accept" : "application/json"
          },
          body:{
            "user_id" :  userData['id'].toString(),
            "first_name" : _nameController.text,
            "phone" : _phoneController.text,
            "email" : _emailController.text,
          }

      );
      final resBody = jsonDecode(res.body);
      print(res.body);
      loadingUpdateUser = false;

      if(res.statusCode == 200){
        setState(() {
          userData = resBody['user'];
        });

      }
    }
    SweetAlert.show(context,
        title: 'Успешно ',
        subtitle: 'Профиль обновлен',
        style: SweetAlertStyle.success
    );

  }

  Future<dynamic> getUser()async{


    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print(token);
    final res = await http.get('$endpointApi/user',headers: {
      "Authorization" : "Bearer $token"
    });

    var jsonResponseNew = jsonDecode(res.body);
    setState(() {
      userData = jsonResponseNew;
      _nameController.text = userData['name'];
      _phoneController.text = userData['phone'];
      _emailController.text = userData['email'];
    });

    final resUsers = await http.get('$endpointApi/sanctum/sellers?shop_id=${userData['shop_id']}',headers: {
      "Authorization" : "Bearer $token"
    });
    final responseJson = jsonDecode(resUsers.body);
    print(responseJson);
    setState(() {
      sellers = responseJson['users'];
    });
    print(userData);
    return 'success';
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getUser();
  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return AppPage(
        content: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 24,left: 16,bottom: 24),
                child: Container(
                  width: MediaQuery.of(context).size.width*0.8,
                  child: Text('Профиль',style: TextStyle(
                      color: Colors.black,
                      fontSize: 30
                  ),),
                ),
              ),
              Container(
                  child: Column(
                      children: [
                        Center(
                          child: userData != null ? (userData['role_id']==1 && sellers.length != 0) ?  ButtonSells(
                            text:hideSellers == true ? 'Показать продавцов' : 'Скрыть продавцов',
                            width: w-60,
                            height: 50.0,
                            onTap: (){
                              audioPlay();

                              setState(() {
                                hideSellers = !hideSellers;
                              });
                            },
                            color: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            borderColor: Theme.of(context).primaryColor,
                            spreadRadius: 0.0,
                            enableBlur: true ,
                            fontSize: 16.0,
                          ): null: Center(child: CircularProgressIndicator()),
                        ) ,
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          width: w-60,

                          child:hideSellers != true ? sellers.length != 0 ?
                          ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: sellers.length,
                              itemBuilder: (context,index)=>
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color:Colors.grey,width: 2),
                                    borderRadius: BorderRadius.circular(10)
                                  ),
                                  margin: const EdgeInsets.only(bottom: 10),
                                  padding: const EdgeInsets.only(top: 10,left: 5,bottom: 10),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('Имя : ${sellers[index]['name']}',style: TextStyle(fontSize: 20),),
                                      Text('Телефон : ${sellers[index]['phone']}',style: TextStyle(fontSize: 20),),
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        child: ButtonSells(
                                          textColor: Colors.white,
                                          onTap: (){
                                            audioPlay();

                                            this.deleteUser(sellers[index]['id']);
                                          },
                                          width: w-30,
                                          height: 40.0,
                                          enableBlur: false,
                                          spreadRadius: 0.0,
                                          color: Colors.red,
                                          fontSize: 15.0,
                                          borderColor: Colors.red,
                                          text: 'Удалить',
                                        ),
                                      )
                                    ],
                                  ),

                                )
                              ): null : SizedBox.shrink(),


                        ),
                        SizedBox(height: 20,),
                        CustomTextField(

                          headText: 'Ваше ФИО',
                          hintText: 'Орлов Иван Николаевич',
                          textCtr: _nameController,
                        ),
                        SizedBox(height: 30,),
                        CustomTextField(headText: 'Ваш номер телефона',
                          formatter: maskFormatter,
                          hintText: '+7',
                          textCtr: _phoneController,
                          keyboard: TextInputType.phone,

                        ),
                        SizedBox(height: 30,),
                        CustomTextField(headText: 'Ваша электронная почта',
                          hintText: 'info@mail.com',
                          textCtr: _emailController,
                        ),

                        SizedBox(height: 30,),
                        CustomTextField(headText: 'Ваш пароль',
                          hintText: '',
                          obscure: true,
                          textCtr: _passwordController,


                        ),

                        SizedBox(
                          height: 20,
                        ),
                        Center(
                          child:
                          userData != null && userData['role_id'] == 1 && registerUser == false ?

                          ButtonSells(
                            text:loadingUpdateUser != false ? '' : 'Обновить профиль',
                            width: w-60,
                            height: 50.0,
                            onTap: (){
                              setState(() {
                                audioPlay();

                                this.updateUser();
                              });
                            },
                            color: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            borderColor: Theme.of(context).primaryColor,
                            spreadRadius: 0.0,
                            enableBlur: true ,
                            fontSize: 16.0,
                          ) : null,
                        ),
                        SizedBox(height: 20,),
                        Center(
                          child:userData != null && userData['role_id'] == 1 && registerUser == false ?  ButtonSells(
                            text:'Зарегистрировать продавца',
                            width: w-60,
                            height: 50.0,
                            onTap: (){
                              audioPlay();

                              setState(() {
                                registerUser = true;
                              });
                            },
                            color: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            borderColor: Theme.of(context).primaryColor,
                            spreadRadius: 0.0,
                            enableBlur: true ,
                            fontSize: 16.0,
                          ) : null,
                        ),
                        Center(
                          child:registerUser == true ? Column(
                            children: [
                              SizedBox(height: 30,),
                              CustomTextField(headText: 'Имя продавца',
                                hintText: '',
                                obscure: false,
                                textCtr: _newUserNameController,

                              ),
                              SizedBox(height: 30,),
                              CustomTextField(headText: 'Телефон продавца',
                                hintText: '',
                                obscure: false,
                                textCtr: _newUserPhoneController,
                                formatter: maskFormatterNewUser,
                                keyboard: TextInputType.phone,
                              ),
                              SizedBox(height: 30,),
                              CustomTextField(headText: 'Пароль продавца',
                                hintText: '',
                                obscure: true,
                                textCtr: _newUserPasswordController,
                              ),

                              SizedBox(height: 20,),
                              Center(
                                child: ButtonSells(
                                  text:'Отменить',
                                  width: w-60,
                                  height: 50.0,
                                  color: Colors.grey,
                                  textColor: Colors.white,
                                  borderColor: Colors.grey,
                                  spreadRadius: 0.0,
                                  enableBlur: true ,
                                  fontSize: 16.0,
                                  onTap: (){
                                    audioPlay();

                                    setState(() {
                                      _newUserPasswordController.text = '';
                                      _newUserPhoneController.text = '';
                                      _newUserNameController.text ='';
                                      registerUser = false;
                                    });
                                  },
                                ),
                              ),
                              SizedBox(height: 20,),

                              Center(
                                child:ButtonSells(

                                  text:loadingRegister == true ? '':'Зарегистрировать продавца',
                                  width: w-60,
                                  height: 50.0,
                                  onTap: () async{
                                    audioPlay();

                                    List<String> errors = [];
                                    if(_newUserPhoneController.text == ''){

                                        errors.add('Введите номер телефона');

                                    }

                                    if(_newUserNameController.text == ''){
                                      errors.add('Введите имя продавца');
                                    }

                                    if(_newUserPasswordController.text == ''){
                                      errors.add('Введите пароль продавца');
                                    }
                                    if(_newUserPasswordController.text.length < 8 && _newUserNameController.text != '' && _newUserPhoneController.text != ''  && _newUserPasswordController.text != ''){
                                      errors.add('Пароль должен быть больше 8 символов');
                                    }
                                    if(errors.length >0){
                                      SweetAlert.show(
                                        context,
                                        title: 'Ошибка',
                                        subtitle: errors.join('\n'),
                                        style: SweetAlertStyle.error,
                                      );
                                    }

                                    setState(() {
                                      loadingRegister = false;
                                    });
                                    final res = await http.post('$endpointApi/sanctum/register',body: {
                                      "name" : _newUserNameController.text,
                                      "phone" : _newUserPhoneController.text,
                                      "password" : _newUserPasswordController.text,
                                      "password_confirmation" : _newUserPasswordController.text,
                                      "device_name" : "Samsung",
                                      "shop_id" : userData['shop_id'].toString()
                                    });
                                    print(res.body);

                                    if(res.statusCode == 200){
                                      setState(() {
                                        registerUser = false;
                                        loadingRegister = false;
                                      });
                                      _newUserPasswordController.text = '';
                                      _newUserNameController.text = '';
                                      _newUserPhoneController.text = '';
                                      this.getUser();
                                      SweetAlert.show(context,
                                          title: 'Успешно ',
                                          subtitle: 'Пользователь зарегистрирован',
                                          style: SweetAlertStyle.success
                                      );

                                    }


                                  },
                                  color: Theme.of(context).primaryColor,
                                  textColor: Colors.white,
                                  borderColor: Theme.of(context).primaryColor,
                                  spreadRadius: 0.0,
                                  enableBlur: true ,
                                  fontSize: 16.0,
                                ),
                              ),
                            ],
                          ) :null,
                        ),
                        SizedBox(height: 200,),

                      ],
                    ),
                  ),
                

            ],
          ),
        )
    );
  }
}
