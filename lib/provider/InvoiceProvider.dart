import 'dart:convert';

import 'package:farcount/provider/AuthProvider.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:farcount/models/Partner.dart';

final endpoint = endpointApi;

PartnerData partners;
Future fetchPartners() async{
  final prefs = await SharedPreferences.getInstance();


  final token = prefs.getString('token');
  try{
    final response = await http.get('${endpoint}/partners',headers: {
      "Authorization" : "Bearer ${token}"
    });



    Map<String,dynamic> data = jsonDecode(response.body);



    if(response.statusCode == 200){
      partners = data['data'];
    }



    return partners;


  }catch(e){

    print(e);

  }
}


