

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class InvoicePage extends StatelessWidget {
  final waybill;
  InvoicePage({Key key , this.waybill}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Text('Накладная номер ${waybill["id"]}',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none
              ),
            ),
            SizedBox(height: 20,),
            Text('Партнер: ${waybill['partner']['name']}',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none
              ),
            ),
            SizedBox(height: 20,),
            Text('Сумма: ${waybill['total_sum']} тг',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none
              ),
            ),
            SizedBox(height: 15,),
            Text('Оплачено: ${waybill['paid']} тг',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none
              ),
            ),
            SizedBox(height: 15,),
            Text('Осталось: ${waybill['total_sum'] - waybill['paid']} тг',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none
              ),
            ),
            SizedBox(height: 15,),
            Text('Товары:',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  decoration: TextDecoration.none
              ),
            ),
            Expanded(

              child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),

                  itemCount: waybill['products'].length,
                  itemBuilder:(context,index){
                    return (ProductContainer(product: waybill['products'][index],));
                  }
              ),
            ),



          ],
        ),
      ),
    );
  }
}


class ProductContainer extends StatelessWidget {
  final product;
  ProductContainer({Key key , this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:20),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black)
        ),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Text('Название: ${product['title']}',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none
                ),
              ),
              SizedBox(height: 10,),
              Text('Количество: ${product['amount']}',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none
                ),
              ),
              SizedBox(height: 10,),

              Text('Цена: ${product['amount']*product['price']}тг',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    decoration: TextDecoration.none
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
