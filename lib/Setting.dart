import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:shared_preferences/shared_preferences.dart';

void audioPlay()async{
  final prefs = await SharedPreferences.getInstance();
  final audio = prefs.getBool('audio');
  print(audio);
  if(audio == null){
    AudioCache audioCache = AudioCache();

    audioCache.play('note1.wav');
    prefs.setBool('audio', true);
  }
  if(audio == true){
    AudioCache audioCache = AudioCache();

    audioCache.play('note1.wav');
  }

}