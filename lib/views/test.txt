FlutterBlue bluetoothManager = FlutterBlue.instance;

  bool _connected = false;
  BluetoothDevice _device;
  String tips = 'no device connect';

  @override
  void initState() {
    super.initState();

    if(this.widget.waybill['total_sum'] is int){
      print(this.widget.waybill['total_sum'].runtimeType);

      this.widget.waybill['total_sum'] = this.widget.waybill['total_sum'].toString();
      this.widget.waybill['paid'] = this.widget.waybill['paid'].toString();

    }
    WidgetsBinding.instance.addPostFrameCallback((_) => initBluetooth());
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initBluetooth() async {
    bluetoothManager.startScan(timeout: Duration(seconds: 4));

    bool isConnected = await bluetoothManager.isAvailable;

    bluetoothManager.state.listen((state) {
      print('cur device status: $state');

      switch (state) {
        case BluetoothManager.CONNECTED:
          setState(() {
            _connected = true;
            tips = 'connect success';
          });
          break;
        case BluetoothManager.DISCONNECTED:
          setState(() {
            _connected = false;
            tips = 'disconnect success';
          });
          break;
        default:
          break;
      }
    });

    if (!mounted) return;

    if (isConnected) {
      setState(() {
        _connected = true;
      });
    }
  }
  List<pw.TableRow> pdfWidgets = [];

  void _onConnect() async {
    audioPlay();

    if (_device != null && _device.address != null) {
      await bluetoothManager.connect(_device);
    } else {
      setState(() {
        tips = 'please select device';
      });
      print('please select device');
    }
  }

  void _onDisconnect() async {
    audioPlay();

    await bluetoothManager.disconnect();
  }

  Future<String> _sendData() async {
    audioPlay();

//    List<int> bytes = latin1.encode('ыфв world!\n\n\n').toList();
    // Set codetable west. Add import 'dart:typed_data';
     List<int> bytes = Uint8List.fromList(List.from('\x1Bt'.codeUnits)..add(6));
    // Text with special characters
     bytes += await CharsetConverter.encode("windows-1251", "Накладная ${this.widget.waybill['id']}\n");

     bytes += await CharsetConverter.encode("windows-1251", "Партнер:    ${this.widget.waybill['partner']['name']}\n");
     bytes += await CharsetConverter.encode("windows-1251", "Общая сумма:    ${int.parse(this.widget.waybill['total_sum']).abs()} тг\n");
     bytes += await CharsetConverter.encode("windows-1251", "Оплачено:    ${int.parse(this.widget.waybill['paid']).abs()} тг\n");
     bytes += await CharsetConverter.encode("windows-1251", "Долг:    ${int.parse(this.widget.waybill['total_sum']).abs()-int.parse(this.widget.waybill['paid']).abs()} тг\n\n\n");
     print(bytes);
//     print(this.widget.waybill['products']);
    int index = 0;
    print(this.widget.waybill['products'].length);
      this.widget.waybill['products'].forEach((product)async{
//       print(product['title']);
      print(index);
       bytes += await CharsetConverter.encode("windows-1251", "Название товара:    ${product['title']} \n");
       bytes += await CharsetConverter.encode("windows-1251", "Артикул:    ${product['articul']} \n");
       bytes += await CharsetConverter.encode("windows-1251", "Количество:    ${product['amount'].abs()} \n");
       bytes += await CharsetConverter.encode("windows-1251", "Цена за штуку:    ${product['price'].abs()} тг\n");
       bytes += await CharsetConverter.encode("windows-1251", "Cумма:    ${product['price'].abs()*product['amount'].abs()} тг\n\n\n");
      index +=1;

       print(this.widget.waybill['products'].length);
       if((this.widget.waybill['products'].length-1) == index){
         print('wdw');


       }



      });






    try{
    }catch(e){
      print('wwewe $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Распечатать накладную'),
      ),
      body: RefreshIndicator(
        onRefresh: () =>
            bluetoothManager.startScan(timeout: Duration(seconds: 4)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 400,
                child: Center(
                  child:PdfPreview(
                    allowPrinting: false,
                    build: (format)async{
                      final doc = pw.Document();


                      final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
                      final ttf = pw.Font.ttf(font);
                      pdfWidgets = [];
                      this.widget.products.forEach((element) {
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Название товара',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['title']}',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Артикул',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['articul']}',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Количество',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['amount'].abs()}',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Цена',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['price'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Общая цена',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['amount'].abs()*element['price']} тг',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );

                      });

                      doc.addPage(
                        pw.Page(
                            build: (pw.Context context) =>pw.Column(
                                children: [
                                  pw.Table(
                                      border:pw.TableBorder.all(color: PdfColor.fromHex('#fff')),
                                      children: [
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Накладная ${this.widget.waybill['id']}',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Партнер ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                              pw.Text('${this.widget.waybill['partner']['name']} ',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Общая сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                              pw.Text('${int.parse(this.widget.waybill['total_sum']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),

                                        pw.TableRow(
                                            children: [
                                              pw.Text('Оплачено ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                              pw.Text('${int.parse(this.widget.waybill['paid']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Осталось ',style: pw.TextStyle(font: ttf,fontSize: 16)),

                                              pw.Text(' ${int.parse(this.widget.waybill['total_sum']).abs()-int.parse(this.widget.waybill['paid']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                            ]
                                        ),



                                      ]
                                  ),
                                  pw.Padding(

                                    child: pw.Table(
                                        border:pw.TableBorder.all(color: PdfColor.fromHex('#fff')),
                                        children: pdfWidgets
                                    ),
                                    padding: const pw.EdgeInsets.only(top: 20),
                                  )
                                ]
                            )
                        ),
                      );
                      return doc.save();
                    },


                  ) ,
                ),
              ),

              SingleChildScrollView(
                child: Column(
                  children: <Widget>[

                    StreamBuilder<List<BluetoothDevice>>(
                      stream: bluetoothManager.scanResults,
                      initialData: [],
                      builder: (c, snapshot) => Column(
                        children: snapshot.data
                            .map((d) => ListTile(
                          title: Text(d.name ?? ''),
                          subtitle: Text(d.address),
                          onTap: () async {
                            setState(() {
                              _device = d;
                            });
                          },
                          trailing:
                          _device != null && _device.address == d.address
                              ? Icon(
                            Icons.check,
                            color: Colors.green,
                          )
                              : null,
                        ))
                            .toList(),
                      ),
                    ),
                    Divider(),
                    Container(
                      padding: EdgeInsets.fromLTRB(20, 5, 20, 10),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              OutlineButton(
                                child: Text('Подключиться'),
                                onPressed: _connected ? null : _onConnect,
                              ),
                              SizedBox(width: 10.0),
                              OutlineButton(
                                child: Text('Отключиться'),
                                onPressed: _connected ? _onDisconnect : null,
                              ),
                            ],
                          ),
                          OutlineButton(
                            child: Text('Распечатать'),
                            onPressed: _connected ? _sendData : null,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: StreamBuilder<bool>(
        stream: bluetoothManager.isScanning,
        initialData: false,
        builder: (c, snapshot) {
          if (snapshot.data) {
            return FloatingActionButton(
              child: Icon(Icons.stop),
              onPressed: () => bluetoothManager.stopScan(),
              backgroundColor: Colors.red,
            );
          } else {
            return FloatingActionButton(
                child: Icon(Icons.search),
                onPressed: () =>
                    bluetoothManager.startScan(timeout: Duration(seconds: 4)));
          }
        },
      ),
    );
  }