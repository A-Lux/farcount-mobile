import 'dart:convert';

import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/utils/bottomNabBar.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:farcount/views/Sales/History.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../Setting.dart';

class HistoryStockPage extends StatefulWidget {
  static var id = 'historyStock';

  @override
  _HistoryStockPageState createState() => _HistoryStockPageState();
}

class _HistoryStockPageState extends State<HistoryStockPage> {
  List<dynamic> debtors;
  List<Widget> debtorsWidget;
  String sortType = 'date';
  String sortValue = 'desc';
  bool paid = false;
  bool notPaid = false;
  bool loading = false;
  TextEditingController searchController = TextEditingController();
  Future<dynamic> getDebtors(sortType,sortValue)async{
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    var res = await
    http.get('${endpointApi}/waybills?sort_type=${sortType}&is_debt=1&is_paid=${paid == true ? 1 : 0}&not_paid=${notPaid == true ? 1:0}&sort_value=${sortValue}&${searchController.text != '' ?'term='+ searchController.text : ''}',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"

    },);
    print('${endpointApi}/waybills?sort_type=${sortType}&is_debt=1&is_paid=${paid == true ? 1 : 0}&not_paid=${notPaid == true ? 1:0}&sort_value=${sortValue}&${searchController.text != '' ?'term='+ searchController.text : ''}');
    if(searchController.text != null){

    }
    var w = MediaQuery.of(context).size.width;
    print(res.body);
    var responseBody = jsonDecode(res.body)['data'];

    setState(() {
      debtors = responseBody;
      loading = false;


    });

    return 'success';
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getDebtors('date','desc');

  }
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return AppPage(

      content: RefreshIndicator(
        key: refreshKey,
        onRefresh: ()async{
          this.getDebtors(sortType, sortValue);
        },
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),

          child: Container(
            margin: const EdgeInsets.only(top: 0.0),
            width: w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child:
                  Text('История',style: TextStyle(fontSize: 30),),
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Container(
                    child: Container(
                      width: w -32,
                      height: 70,
                      child: TextField(
                        controller: searchController,
                        cursorColor: Colors.black,
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                            borderRadius: new BorderRadius.circular(8.0),
                          ),
                          hintText: 'Поиск по накладным',
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                            borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                          ),
                          suffixIcon: Icon(Icons.search,color: Color(0xffDFE0E3)),
                        ),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          color: Colors.black,
                        ),
                        onChanged: (val){
                          this.getDebtors(sortType, sortValue);
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text('Сортировать по:',style: TextStyle(
                    color: Color(0xffC2CACD),
                    fontSize: 14,
                  ),),
                ),
                SizedBox(height: 8,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(children: [
                    GestureDetector(
                      onTap:(){
                        audioPlay();

                        setState(() {

                          if(sortType == 'date'){
                            sortValue == 'desc' ? sortValue = 'asc': sortValue = 'desc';

                          }else{
                            sortType = 'date';

                          }
                          this.getDebtors('date',sortValue);
                        });
                      },
                      child: Container(
                        width: w/3 - 24,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: sortType == 'date' ? Theme.of(context).primaryColor : Color(0xffffffff),
                            border: Border.all(color: Theme.of(context).primaryColor),

                            boxShadow: [
                              BoxShadow(
                                color:  Theme.of(context).primaryColor.withOpacity(0.5),
                                spreadRadius:sortType == 'date' ? 2 : 0,
                                blurRadius:sortType == 'date' ? 8 : 0,
                                offset: sortType == 'date' ?Offset(0, 4):Offset(0, 0), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment:sortType == 'date' ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.center,
                            children: [
                              Text('Дате ',style: TextStyle(
                                  fontSize: 16,
                                  color: sortType == 'date' ? Color(0xffffffff) : Colors.black
                              ),),

                              sortType == 'date' ? Icon(sortValue=='desc' ? Icons.arrow_downward : Icons.arrow_upward,color:sortType == 'date' ? Color(0xffffffff) : Colors.black,) : SizedBox.shrink(),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16,),
                    GestureDetector(
                      onTap: (){
                        audioPlay();

                        setState(() {
                          if(sortType == 'price'){
                            sortValue == 'desc' ? sortValue = 'asc': sortValue = 'desc';

                          }else{
                            sortType = 'price';

                          }
                          this.getDebtors('total_sum',sortValue);
                        });
                      },
                      child: Container(
                        width: w/3 - 24,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: sortType == 'price' ? Theme.of(context).primaryColor : Color(0xffffffff),
                            border: Border.all(color: Theme.of(context).primaryColor),
                            boxShadow: [
                              BoxShadow(
                                color: Theme.of(context).primaryColor.withOpacity(0.5),
                                spreadRadius:sortType == 'price' ? 2 : 0,
                                blurRadius:sortType == 'price' ? 8 : 0,
                                offset:sortType == 'price' ? Offset(0, 4): Offset(0, 0), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment:sortType == 'price' ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.center,

                            children: [
                              Text('Цене',style: TextStyle(
                                  fontSize: 16,
                                  color: sortType == 'price' ? Color(0xffffffff):Colors.black
                              ),),
                              sortType == 'price' ? Icon(sortValue=='desc' ? Icons.arrow_downward : Icons.arrow_upward,color:sortType == 'price' ? Color(0xffffffff) : Colors.black,) : SizedBox.shrink(),

                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16,),
                    GestureDetector(
                      onTap: (){
                        audioPlay();

                        setState(() {
                          if(sortType == 'name'){
                            sortValue == 'desc' ? sortValue = 'asc': sortValue = 'desc';

                          }else{
                            sortType = 'name';

                          }
                          this.getDebtors('name',sortValue);
                        });
                      },
                      child: Container(
                        width: w/3 - 24,
                        height: 50,

                        decoration: BoxDecoration(
                            color: sortType == 'name' ? Theme.of(context).primaryColor : Color(0xffffffff),
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: Theme.of(context).primaryColor),
                            boxShadow: [
                              BoxShadow(
                                color:  Theme.of(context).primaryColor.withOpacity(0.5),
                                spreadRadius:sortType == 'name' ? 2 : 0,
                                blurRadius:sortType == 'name' ? 8 : 0,
                                offset: sortType == 'name' ? Offset(0, 4) : Offset(0, 0), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment:sortType == 'name' ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.center,

                            children: [

                              Text('Имени',style: TextStyle(
                                  fontSize: 16,
                                  color: sortType == 'name' ? Color(0xffffffff):Colors.black
                              ),),
                              sortType == 'name' ? Icon(sortValue=='desc' ? Icons.arrow_downward : Icons.arrow_upward,color:sortType == 'name' ? Color(0xffffffff) : Colors.black,) : SizedBox.shrink(),

                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                  ),
                ),
                SizedBox(height: 16,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:16.0,vertical: 20),
                  child: Row(
                    mainAxisAlignment:MainAxisAlignment.start ,
                    children: [
                      Row(
                        children: [
                          GestureDetector(
                            onTap:(){
                              audioPlay();

                              setState(() {
                                paid = !paid;
                                this.getDebtors(sortType, sortValue);
                              });
                            },
                            child: Container(
                              width: 36,
                              height: 36 ,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xff1FBCFF)),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child:paid == true ?  Icon(Icons.check,color: Color(0xff1FBCFF),) : Text(''),
                            ),
                          ),
                          SizedBox(width: 15,),
                          Text('Оплаченные',style: TextStyle(fontSize: 18),)
                        ],
                      ),
                      SizedBox(width: 30,),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: (){
                              audioPlay();

                              setState(() {
                                notPaid = !notPaid;
                                this.getDebtors(sortType, sortValue);

                              });
                            },
                            child: Container(
                              width: 36,
                              height: 36 ,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xff1FBCFF)),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child: notPaid == true ?  Icon(Icons.check,color: Color(0xff1FBCFF),) : Text(''),
                            ),
                          ),
                          SizedBox(width: 15,),
                          Text('Не оплаченные',style: TextStyle(fontSize: 18),)
                        ],
                      ),


                    ],
                  ),
                ),

                 Container(width: w,
                    child:debtors == [] ? Center(child:Text('Пока что накладных нет')) :
                    loading == false ? Column(
                      children: <Widget>[
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context,i){

                          return GestureDetector(
                            onTap: (){
                              audioPlay();

                              Navigator.push(context,MaterialPageRoute(builder: (context)=>OwePersonView(waybill: debtors[i]['id'],)));
                            },
                            child: Container(
                              height: 80,
                              width: w,
                              decoration: BoxDecoration(
                                border: i == 2 ? Border.symmetric(vertical: BorderSide(color:Color(0xffDFE0E3)))  : Border(top: BorderSide(color: Color(0xffDFE0E3),)),
                              ),
                              padding: EdgeInsets.symmetric(horizontal: 16,vertical: 8),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text('#${debtors != null ? debtors[i]['id'] : i+1}',style: TextStyle(
                                        color: Color(0xffC2CACD),
                                        fontSize: 12,
                                      ),),
                                      SizedBox(width: 16,),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('${debtors != null ? debtors[i]['partner_id'] : ''}',style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18,
                                          ),),
                                          SizedBox(
                                            height: 8,
                                          ),
                                          Row(
                                            children: [
                                              PriceContainer(debtors != null ? debtors[i]['total_sum'] : 0,debtors != null ? debtors[i]['paid'] : 0),
                                              SizedBox(width: 16,),
                                              Text('${debtors != null ? '${DateTime.parse(debtors[i]['created_at']).day}.${DateTime.parse(debtors[i]['created_at']).month}.${DateTime.parse(debtors[i]['created_at']).year}' : null} ',
                                                style: TextStyle(
                                                    color: Color(0xffC2CACD),
                                                    fontSize: 14
                                                ),)
                                            ],
                                          )
                                        ],
                                      )
                                    ],

                                  ),
                                  Positioned(
                                      right:100,
                                      child:Padding(
                                        padding: const EdgeInsets.only(bottom:20.0),
                                        child: Text('.',style: TextStyle(fontSize: 50,color:debtors != null && debtors[i]['total_sum']-debtors[i]['paid'] != 0 ?  Colors.red : Colors.green),),
                                      )
                                  ),
                                  Positioned(
                                    right: 0,
                                    child: GestureDetector(
                                        onTap: (){
                                          Navigator.pushNamed(context, OwePersonView.id);
                                        },
                                        child: Icon(Icons.navigate_next,color: Color(0xffC2CACD),size: 40,)),
                                  )
                                ],
                              ),
                            ),
                          );
                        },itemCount: debtors != null ? debtors.length : 1  ,),
                      ],
                    ) : Container(
                      padding: EdgeInsets.all(50),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),

                    ),
                  ),



              ],
            ),
          ),
        ),
      ),

    );
  }

}
