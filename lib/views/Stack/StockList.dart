import 'package:farcount/utils/bottomNabBar.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/material.dart';
class LendersList extends StatelessWidget {
  static var id = 'lendorsList';
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xffffffff),
      appBar: AppBar(title: Text(
          'Список кредиторов'
      ),
        elevation: 1,
        centerTitle: true,),
      body: Container(
        width: w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                child: Container(
                  width: w -32,
                  height: 70,
                  child: TextField(
                    cursorColor: Colors.black,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                        borderRadius: new BorderRadius.circular(8.0),
                      ),
                      hintText: 'Поиск по накладным',
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(8.0),
                        borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                      ),
                      suffixIcon: Icon(Icons.search,color: Color(0xffDFE0E3)),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    style: new TextStyle(
                      color: Colors.black,
                    ),
                    onChanged: (val){

                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 16,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text('Сортировать по:',style: TextStyle(
                color: Color(0xffC2CACD),
                fontSize: 14,
              ),),
            ),
            SizedBox(height: 8,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(children: [
                Container(
                  width: w/2 - 24,
                  height: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Theme.of(context).primaryColor,
                      boxShadow: [
                        BoxShadow(
                          color:  Theme.of(context).primaryColor.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 8,
                          offset: Offset(0, 4), // changes position of shadow
                        ),
                      ]
                  ),
                  child: Center(
                    child: Text('Cумме долга',style: TextStyle(
                        fontSize: 16,
                        color: Colors.white
                    ),),
                  ),
                ),
                SizedBox(width: 16,),
                Container(
                  width: w/2 - 24,
                  height: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Theme.of(context).primaryColor)
                  ),
                  child: Center(
                    child: Text('Имени',style: TextStyle(
                        fontSize: 16,
                        color: Colors.black
                    ),),
                  ),
                ),
              ],),
            ),
            SizedBox(height: 16,),
            Expanded(
              child: Container(width: w,
                child: ListView.builder(itemBuilder: (context,i){
                  return GestureDetector(
                    onTap: ()=>{

                      Navigator.pushNamed(context, OwePersonView.id)
                    },
                    child: Container(
                      height: 80,
                      width: w,
                      decoration: BoxDecoration(
                        border: i == 2 ? Border.symmetric(vertical: BorderSide(color:Color(0xffDFE0E3)))  : Border(top: BorderSide(color: Color(0xffDFE0E3),)),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 16,vertical: 8),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('#1235',style: TextStyle(
                                color: Color(0xffC2CACD),
                                fontSize: 12,
                              ),),
                              SizedBox(width: 16,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Александров Николай',style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    children: [
                                      PriceContainer(),
                                      SizedBox(width: 16,),
                                      Text('18.10.2020',
                                        style: TextStyle(
                                            color: Color(0xffC2CACD),
                                            fontSize: 14
                                        ),)
                                    ],
                                  )
                                ],
                              )
                            ],

                          ),
                          Positioned(
                            right: 0,
                            child: GestureDetector(
                                onTap: (){
                                  Navigator.pushNamed(context, OwePersonView.id);
                                },
                                child: Icon(Icons.navigate_next,color: Color(0xffC2CACD),size: 40,)),
                          )
                        ],
                      ),
                    ),
                  );
                },itemCount: 3,),
              ),
            )

          ],
        ),
      ),

    );
  }
}
class PriceContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Color(0xffEBF9FF),
      ),
      child: Center(child:
      Text(
        '3500 ₸',style: TextStyle(
          color: Theme.of(context).primaryColor,
          fontSize: 14
      ),
      ),),
    );
  }
}
