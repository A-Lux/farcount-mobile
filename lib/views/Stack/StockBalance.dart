import 'dart:convert';

import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

import '../../Setting.dart';
class StockBalancePage extends StatefulWidget {
  static var id = 'stock_balancem';
  @override
  _StockBalancePageState createState() => _StockBalancePageState();
}

class _StockBalancePageState extends State<StockBalancePage> {
  List<String> months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];
  TextEditingController term = TextEditingController();
  String sortType = 'cost';
//  String sortValue = 'desc';
  String mainSum = '0';
  bool loading = false;
  bool profitLoader = false;
  String mainAmount = '0';
  String profitSum = '0';
  DateTime lastDate = DateTime.now();
  DateTime initialDate = DateTime.now().subtract(new Duration(days: 30));
  List<dynamic> products;
  Future<dynamic> getDebtors()async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
    });

    final token = prefs.getString('token');
    print(token);
    var res = await http.get('$endpointApi/processed_products/remainders?sort_type=$sortType&sort_value=${sortType == 'title' ? 'asc': 'desc'}${term.text == '' ? '': '&term='+term.text}',
      headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"
      },
    );
    print('$endpointApi/processed_products/remainders?sort_type=$sortType&sort_value=${sortType == 'title' ? 'asc': 'desc'}${term.text != '' ? '': '&term='+term.text}');
    var w = MediaQuery.of(context).size.width;
    print(res.body.toString());
    var responseBody = jsonDecode(res.body);


    setState(() {
      products = responseBody['products'];
      mainSum = responseBody['total']['price'].toString();
      mainAmount = responseBody['total']['amount'].toString();
      loading = false;

      var i = 1;


    });

    return 'success';
  }
  Future<dynamic> getProfit()async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      profitLoader = true;
    });

    final token = prefs.getString('token');
    print(token);
    var res = await http.get('$endpointApi/processed_products/profit?date[]=$initialDate&date[]=$lastDate',
      headers: {
        "Accept" : "Application/json",
        "Authorization" : "Bearer $token"
      },
    );
    var w = MediaQuery.of(context).size.width;
    print(res.body.toString());
    var responseBody = jsonDecode(res.body);
    print(responseBody);
    setState(() {
      profitSum = responseBody['profit'].round().toString();
      profitLoader = false;
      var i = 1;


    });

    return 'success';
  }

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getDebtors();
    this.getProfit();
  }
  @override

  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Остатки на складе'),
        elevation: 1,
      ),
      body: RefreshIndicator(
        key: refreshKey,
        onRefresh: ()async{
          this.getDebtors();
          this.getProfit();
        },
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),

          child: Container(
            width: w,
            child:  Column(
              children: [
                Container(
                  color: Color(0xffF2FBFF),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal:16.0,vertical: 20),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [

                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Количество товара',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400,color: Colors.black),),
                                SizedBox(height: 10,),
                                Text('$mainAmount шт',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500,color: Colors.black),),

                              ],
                            ),
                            SizedBox(width:70 ,),

                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Сумма',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400,color: Colors.black),),
                                SizedBox(height: 10,),
                                Text('$mainSum ₸ ',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500,color: Colors.black),),

                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16,vertical: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Container(
                          width: w -32,
                          height: 70,
                          child: TextField(
                            controller: term,
                            cursorColor: Colors.black,
                            decoration: new InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                borderRadius: new BorderRadius.circular(5.0),
                              ),
                              hintText: 'Поиск по товарам и артикулам',

                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(5.0),
                                borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                              ),
                              suffixIcon: Icon(Icons.search,color: Color(0xffDFE0E3)),
                            ),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              color: Colors.black,
                            ),
                            onChanged: (val){
                              setState(() {
                                this.getDebtors();
                              });
                            },
                          ),
                        ),
                        SizedBox(height: 10,),
                        Text('Сортировать по:',style: TextStyle(
                          color: Color(0xffC2CACD),
                          fontSize: 14,
                        ),),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ButtonSells(
                              text:'по себестоимости',
                              width: w/2-20,
                              height: 50.0,
                              onTap: (){
                                audioPlay();

                                setState(() {
                                  sortType = 'cost';
                                  this.getDebtors();
                                });
                              },
                              color: sortType == 'cost' ? Theme.of(context).primaryColor : Colors.white,
                              textColor: sortType == 'cost' ? Colors.white : Colors.black,
                              borderColor: Theme.of(context).primaryColor,
                              spreadRadius: 0.0,
                              enableBlur: sortType == 'cost' ? true : false,
                              fontSize: 16.0,

                            ),
                            ButtonSells(
                              text:'по рознице',
                              width: w/2-20,
                              height: 50.0,
                              onTap: (){
                                audioPlay();

                                setState(() {
                                  sortType = 'price';
                                  this.getDebtors();
                                });
                              },
                              color: sortType == 'price' ? Theme.of(context).primaryColor : Colors.white,
                              textColor: sortType == 'price' ? Colors.white : Colors.black,
                              borderColor: Theme.of(context).primaryColor,
                              spreadRadius: 0.0,
                              enableBlur: sortType == 'price' ? true : false,
                              fontSize: 16.0,

                            )
                          ],
                        ),
                        SizedBox(height: 15,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ButtonSells(
                              text:'Имени',
                              width: w/3-20,
                              height: 50.0,
                              onTap: (){
                                audioPlay();

                                sortType = 'title';
                                this.getDebtors();
                              },
                              color: sortType == 'title' ? Theme.of(context).primaryColor : Colors.white,
                              textColor: sortType == 'title' ? Colors.white : Colors.black,
                              borderColor: Theme.of(context).primaryColor,
                              spreadRadius: 0.0,
                              enableBlur: sortType == 'title' ? true : false,
                              fontSize: 16.0,

                            ),
                            ButtonSells(
                              text:'Количеству',
                              width: w/3-20,
                              height: 50.0,
                              onTap: (){
                                audioPlay();

                                sortType = 'amount';
                                this.getDebtors();
                              },
                              color: sortType == 'amount' ? Theme.of(context).primaryColor : Colors.white,
                              textColor: sortType == 'amount' ? Colors.white : Colors.black,
                              borderColor: Theme.of(context).primaryColor,
                              spreadRadius: 0.0,
                              enableBlur: sortType == 'amount' ? true : false,
                              fontSize: 16.0,

                            ),
                            ButtonSells(
                              text:'Сумме',
                              width: w/3-20,
                              height: 50.0,
                              onTap: (){
                                audioPlay();

                                this.sortType = 'sum';
                                this.getDebtors();
                              },
                              color: sortType == 'sum' ? Theme.of(context).primaryColor : Colors.white,
                              textColor: sortType == 'sum' ? Colors.white : Colors.black,
                              borderColor: Theme.of(context).primaryColor,
                              spreadRadius: 0.0,
                              enableBlur: sortType == 'sum' ? true : false,
                              fontSize: 16.0,

                            )
                          ],
                        ),
                        SizedBox(height: 20,),



                      ],
                    ),
                  ),
                ),
                Container(
                  width: w,
                  child:loading != true ? products != null ? Column(
                    children: <Widget>[

                      ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (context,i){
                        return InfoTile(product: products[i],isLast: i == 2,i: i,color: 0xffffffff,);
                      },itemCount: products.length,),
                    ],
                  ) : Center(child: Text('Продуктов нет'),) :
                  Container(
                    padding: EdgeInsets.all(100),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
                SizedBox(height: 30,),
//              Container(
//                child: Center(
//                  child: ButtonSells(
//                    text:'Показать еще',
//                    width: w-32,
//                    height: 65.0,
//                    onTap: (){},
//                    color: Colors.white,
//                    textColor: Colors.black,
//                    spreadRadius: 0.0,
//                    borderColor: Colors.grey,
//                    enableBlur: true,
//                  ),
//                ),
//
//              ),
                SizedBox(height: 30,),
                Container(
                  width: w,
                  color: Color(0xffF2FBFF),
                  padding: EdgeInsets.symmetric(horizontal: 16,vertical:22),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Text(
                          'Маржа складских остатков',
                          style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),


                        ),
                      ),
                      SizedBox(height: 20,),
                      GestureDetector(
                        onTap: () async{
                          final List<DateTime> picked = await DateRagePicker.showDatePicker(
                              context: context,
                              initialFirstDate: initialDate,
                              initialLastDate: lastDate,
                              firstDate: new DateTime(2015),
                              lastDate: new DateTime(2050));

                          if(picked != null && picked.length == 2){
                            setState(() {
                              initialDate = picked[0];
                              lastDate = picked[1];
                              this.getProfit();
                            });
//                          this.getDebtors([picked]);
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text('Выберите период:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  '${initialDate.day} ${months[initialDate.month-1]}- ${lastDate.day} ${months[lastDate.month-1]}',
                                  style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor
                                  ),
                                ),
                                SizedBox(width: 16,),
                                Icon(Icons.calendar_today,color: Theme.of(context).accentColor,),
                              ],
                            )

                          ],
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        height: 60,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0xffC7EDFE)),
                          color:Colors.white,
                          borderRadius: BorderRadius.circular(9)
                        ),
                        child:profitLoader != true ? Center(
                          child:Text('$profitSum ₸',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),)
                        ) : Center(
                          child: CircularProgressIndicator(),
                        ),
                      )
                    ],

                  ),
                ),
                SizedBox(height: 40,)





              ],
            ),
          ),
        ),
      ),
    );
  }
}
