
import 'dart:convert';

import 'package:farcount/models/Partner.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/provider/InvoiceProvider.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:farcount/views/answers/Success.dart';
import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/date_picker.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:http/http.dart' as http;
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:core';

import 'package:sweetalert/sweetalert.dart';

import '../../Setting.dart';

class InvoiceCreatePage extends StatefulWidget {
  final Map<String,dynamic> waybill;
  final bool isSaleInvoice;


  static var id = 'create_invoice';

  const InvoiceCreatePage({Key key, this.waybill, this.isSaleInvoice}) : super(key: key);
  @override
  _InvoiceCreatePageState createState() => _InvoiceCreatePageState();
}



class _InvoiceCreatePageState extends State<InvoiceCreatePage> {
  Map<String,dynamic> waybill;
  bool onTapLoading = false;

  final _formMain = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  TextEditingController _phoneController = new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');
  final _productNameController = TextEditingController();
  final _vendorCodeController = TextEditingController();
  final _quantityProduct = TextEditingController();
  final _priceController = TextEditingController();
  final _sumController = TextEditingController();
  final _paidSumController = TextEditingController();
  final _debtController = TextEditingController();
  String _time;
  String _date;
  String _partnerId;
  String _productId;
  int mainSum;
  int waybillId;
  var shopId;
  var editableProduct;
  bool loading = false;
  String searchType;
  String searchValue;
  bool showPartnerDropdown = false;
  Future partners;
  List<Widget> productsWidget = new List<Widget>();
  List<dynamic> products = new List<dynamic>();
  List<dynamic> searchedProducts = [];
  @override
  void dispose() {
    // TODO: implement dispose
    _nameController.dispose();
    _phoneController.dispose();
    super.dispose();
  }
  List data = List(); //edited line
  List productsList = List(); //edited line
  final nameFocus = FocusNode();
  final articleFocus = FocusNode();
  void selectProduct(product){
    _productNameController.text = product['title'];
    _vendorCodeController.text = product['articul'];
    if(this.widget.isSaleInvoice != true) {
      _priceController.text = product['price'].toString();
    }
    _productId = product['id'].toString();
    showProductDropdown = false;
    showVendorDropdown = false;
  }

  Future<dynamic> getPartners({onTap = null}) async {
    final prefs = await SharedPreferences.getInstance();


    final token = prefs.getString('token');
    var res = await http
        .get('${endpointApi}/partners', headers: {
          "Accept": "application/json",
          "Authorization" : "Bearer $token"

    });
    print(res.body);
    var resBody = json.decode(res.body)['data'];
    print(resBody);
    setState(() {
      data = [];
      if(resBody != null){
        data = resBody;

      }
      if(onTap){
        _partnerId = data[data.length-1]['id'].toString();
        _nameController.text = data[data.length-1]['name'];
      }
    });


    return "Success";
  }
  bool showProductDropdown = false;
  bool showVendorDropdown = false;
  List<dynamic> partnersList = [];
  Future<dynamic> getProcessedProducts(searchWord,isName) async{
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    setState(() {
      loading = true;
    });
    final res = await http.get(
      '$endpointApi/processed_products/search?${isName == 1 ? 'search_type=title': 'search_type=articul'}${searchWord != '' ? '&search_value=$searchWord' : ''}',
      headers:{
        "Accept" : "application/json",
        "Authorization": "Bearer $token"
      }
    );


    setState(() {
      if(isName == 1){
        showProductDropdown = true;
      }else{
        showVendorDropdown = true;
      }

      loading = false;

      final resBody = jsonDecode(res.body);

      if(searchWord == '' || resBody['products'].length == 0){
        showProductDropdown = false;
        showVendorDropdown = false;
      }


      if(resBody['products'] != null){
        searchedProducts = resBody['products'];
      }
    });




  }


  TextEditingController partnerSearchController = TextEditingController();
  Future<dynamic> getPartner(search)async{
    setState(() {
      showPartnerDropdown = true;
      loading = true;

    });

    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final res = await http.get('$endpointApi/partners/search?term=${_nameController.text}',headers:{
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"
    });
    print(res.body);
    final partners = jsonDecode(res.body)['partners'];
    setState(() {
      if(partners.length == 0){
          showPartnerDropdown = false;
      }
      if(partners != null){
        print(partners);
        partnersList = partners;
      }
      loading = false;
    });

    return 'success';
  }
  Future<dynamic> getUser()async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print(token);
    final res = await http.get('$endpointApi/user', headers: {
      "Authorization": "Bearer $token"
    });

    setState(() {
      shopId = jsonDecode(res.body)['shop_id'];
      print(shopId);

    });
  }

  Future<dynamic> getProducts() async {
    final prefs = await SharedPreferences.getInstance();


    final token = prefs.getString('token');
    var res = await http
        .get('${endpointApi}/products', headers: {
          "Accept": "application/json",
      "Authorization" : "Bearer $token"

    });
    var resBody = json.decode(res.body)['data'];
    print(resBody);
    setState(() {

      productsList = [];
      if(resBody != null){
        productsList = resBody;
      }
    });


    return "Sucess";
  }

  
  
  bool deleteProduct(index){
    index = int.parse(index);
    print(index-1);

    final product = this.products[index-1];

    final productWidget = this.productsWidget[index-1];

    setState(() {
      _sumController.text = (int.parse(_sumController.text)-(product['price']*product['amount'])).toString();


      if(_debtController.text != ''){
        _debtController.text = (int.parse(_debtController.text)-(product['price']*product['amount'])).toString();
        if(int.parse(_debtController.text)<0){
          _debtController.text = '0';
        }
      }
      this.products.remove(product);
      this.productsWidget.remove(productWidget);
      SweetAlert.show(context,
          title: 'Успешно',
          subtitle: 'Товар был удален',
          style: SweetAlertStyle.success
      );

    });




    return true;

    print(this.productsWidget);

  }
  var processedProduct;
  String uniqueNameError;
  String uniqueArticulError;
  void checkUnique()async{
    final prefs = await SharedPreferences.getInstance();


    final token = prefs.getString('token');
    var res = await http
        .get('$endpointApi/products/check/unique?term=${_productNameController.text}', headers: {
      "Accept": "application/json",
      "Authorization" : "Bearer $token"

    });
    var resBody = json.decode(res.body)['product'];
    print(_productNameController.text);
    print(resBody);
    if(res.statusCode == 200){
          setState(() {
            uniqueNameError = 'Артикул занят товаром ${resBody['title']} с артикулом ${resBody['articul']}';
          });

    }else{
      setState(() {
        uniqueNameError = null;
      });
    }
  }
  void checkUniqueArticul()async{
    final prefs = await SharedPreferences.getInstance();


    final token = prefs.getString('token');
    var res = await http
        .get('$endpointApi/products/check/unique?term=${_vendorCodeController.text}', headers: {
      "Accept": "application/json",
      "Authorization" : "Bearer $token"

    });
    print(res.statusCode);

    var resBody = json.decode(res.body)['product'];
    if(res.statusCode == 200){


      setState(() {
        uniqueArticulError = 'Артикул занято товаром ${resBody['title']} с артикулом ${resBody['articul']}';
      });

    }else{
      setState(() {
        uniqueArticulError = null;
      });
    }
  }


  bool editProduct(productName,productPrice,vendorCode,quantity,productId,processedProductId){
    setState(() {
      _productNameController.text = productName;
      _vendorCodeController.text = vendorCode;
      _quantityProduct.text = quantity.abs().toString();
      _priceController.text = productPrice.toString();
      editableProduct = productId;
      processedProduct = processedProductId;
      Scrollable.ensureVisible(dataKey.currentContext,curve: Curves.linear,duration: Duration(milliseconds: 500));
    });
    

    print(editableProduct);


    return true;


  }

  final dataKey = new GlobalKey();



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getPartners();
    this.getProducts();
    this.getUser();
    articleFocus.addListener(() {
      if(!articleFocus.hasFocus){
        showVendorDropdown = false;
        FocusScope.of(context).unfocus();

      }
    });
    nameFocus.addListener(() {
      if(!nameFocus.hasFocus){
        showVendorDropdown = false;
        FocusScope.of(context).unfocus();

      }
    });
//    this.getContactsList();



    if(this.widget.waybill != null){
      _sumController.text = this.widget.waybill['total_sum'].abs().toString();
      products = this.widget.waybill['products'];
      var i = 0 ;
      this.widget.waybill['products'].forEach((product)=>{
        product['processed_product_id'] = product['id'],
        productsWidget.add(
          ProductCard(product['title'],product['articul'],(productsWidget.length + 1).toString(), product['price'], product['amount'], deleteProduct, editProduct,product['id'])
        )

      });
      print(this.widget.waybill);
      _partnerId = this.widget.waybill['partner']['id'].toString();
      _nameController.text = this.widget.waybill['partner']['name'];
      waybillId = this.widget.waybill['waybill_id'];
      _paidSumController.text = this.widget.waybill['paid'].abs().toString();
      _debtController.text = (this.widget.waybill['total_sum'].abs() - this.widget.waybill['paid'].abs()).toString();


    }else{
      waybill = null;
    }


  }

  showAlertDialog(BuildContext context,text){
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Успешно"),
      content: Text("$text"),
      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  @override

  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.waybill != null ? 'Накладная #${this.widget.waybill['id']}' :this.widget.isSaleInvoice == true ? 'Создать накладную продажи' : 'Создать накладную поступления'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,

          children: [
            Padding(
              padding: const EdgeInsets.only(top:20.0,left:16,right: 16),
              child: Form(
                key: _formMain,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Text('Данные клиента',style: TextStyle(fontSize: 30,color: Colors.black),),
                    SizedBox(
                      height: 20,
                    ),
                    Container(

                      width: w,
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
//                        child: DropdownButton<String>(
//                              isExpanded: true,
//                              value: _partnerId,
//                              icon: Icon(Icons.arrow_downward,color: Theme.of(context).primaryColor,),
//                              iconSize: 24,
//                              elevation: 16,
//                              style: TextStyle(color:Theme.of(context).primaryColor),
//                              underline: Container(
//                                width: w,
//                                height: 2,
//                                color: Theme.of(context).primaryColor,
//                              ),
//                              onChanged: (String newValue) {
//                                setState(() {
//                                  _partnerId = newValue;
//                                });
//                              },
//                              items: data.map((value) {
//                                return DropdownMenuItem<String>(
//                                  value: value['id'].toString(),
//                                  child: Text(value['name'],style: TextStyle(fontSize: 18),),
//                                  onTap: (){
//
//                                  },
//                                );
//                              }).toList(),
//
//                              ),
                        child: Column(
                          children: [

                          ],
                        )
                      ),


                      ),
                    Padding(
                      padding: EdgeInsets.only(top: 40,left: 15),
                      child: Text("ФИО клиента",style: TextStyle(
                          color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top:15.0),

                      child: TextField(
                        onChanged: (String value){

                          if(value != ''){
                            this.getPartner(value);

                          }else{
                            setState(() {
                              _partnerId = null;
                              showPartnerDropdown = false;

                            });
                          }
                        },
                        controller: _nameController,
                        keyboardType: TextInputType.text,
                        autofocus: false,

                        decoration: InputDecoration(
                          hintText: 'Орлог Николаевич',
                          hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                          border: new OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xffDFE0E3)
                            )
                          ),


                        ),

                      ),

                    ),
                    Container(
                      height: showPartnerDropdown == true ? 200 : 0,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color:Colors.grey,
                              width: showPartnerDropdown == true  ? 2 : 0
                          ),
                          color: Colors.white
                      ),
                      child: loading != true  ? ListView.builder(
                        itemBuilder: (context,i){
                          return
                            GestureDetector(
                              onTap: (){

                                setState(() {
                                  _partnerId = partnersList[i]['id'].toString();
                                  _nameController.text = partnersList[i]['name'];
                                  showPartnerDropdown = false;


                                });
                              },
                              child: Container(
                                padding: const EdgeInsets.all(15),
                                child: Text(partnersList[i]['name'],style: TextStyle(fontSize: 16),),
                                decoration: BoxDecoration(
                                    border: Border(bottom: BorderSide(color: Colors.grey,width: 2))
                                ),
                              ),
                            );
                        },
                        itemCount: partnersList != null ? partnersList.length : 0,
                      ) : Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),


                    this._partnerId == null ? Padding(
                      padding: EdgeInsets.only(top: 40,left: 15),
                      child: Text("Номер телефона клиента",style: TextStyle(
                          color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                    ): SizedBox(height: 0,),
                    this._partnerId == null ? Padding(
                      padding:   const EdgeInsets.only(top:15.0),
                      child: TextField(
                        keyboardType: TextInputType.phone,
                        autofocus: false,
                        controller:_phoneController,
                        decoration: InputDecoration(

                          hintText: '+7',
                          hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                          border: new OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xffDFE0E3)
                              )
                          ),



                        ),
                      ),
                    ) : SizedBox(height: 0,),
                    this._partnerId == null ? Padding(
                      padding: const EdgeInsets.only(top:40),
                      child: GestureDetector(
                        onTap: (){

                        },
                        child:  ButtonSells(
                          text: 'Добавить клиента',
                          onTap:() async{
                            audioPlay();

                            final prefs = await SharedPreferences.getInstance();
                            final token = await prefs.getString('token');
                              if(this._partnerId == null) {
                               try {
                                  final endpoint = endpointApi;
                                  final response = await http.post(
                                      '${endpoint}/partners', body: {
                                    "phone": this._phoneController.text,
                                    "name": this._nameController.text,
                                    "shop_id": this.shopId.toString()
                                  },headers: {
                                    "Authorization" : "Bearer $token"
                                  });
                                  final data = jsonDecode(response.body);

                                  if (response.statusCode == 200) {
                                    _nameController.text = '';
                                    _phoneController.text = '';
                                    SweetAlert.show(context,
                                        title: 'Успешно',
                                        subtitle: 'Партнер создан!',
                                        style: SweetAlertStyle.success
                                    );
                                    setState(() {
                                      print(data['data']);

                                    });
                                  }
                                } catch (e) {
                                  print(e);
                                }

                              setState(() {
                                this.getPartners(onTap: true);
                              });
                            }else{
                              setState(() {
                                this._partnerId = null;
                              });
                            }
                          },
                          width: w-32,
                          color: Theme.of(context).primaryColor,
                          height: 50.0,
                          textColor: Colors.white,
                          fontSize: 18.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur: true,


                        ),
                      ),
                    ) :SizedBox(height: 0,),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: [
//
//                        Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: [
//                            Padding(
//                              padding: EdgeInsets.only(bottom: 10,left: 15),
//                              child: Text("Дата",style: TextStyle(
//                                  color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
//                            ),
//                            Container(
//                              width: w/2-30,
//                              child: TextField(
//
//                                keyboardType: TextInputType.text,
//                                autofocus: false,
//                                decoration: InputDecoration(
//                                  hintText: '18.05.2020',
//                                  hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
//                                  border: new OutlineInputBorder(
//                                      borderSide: BorderSide(
//                                          color: Color(0xffDFE0E3)
//                                      )
//                                  ),
//
//
//                                ),
//                                onTap:
//                                ()=>{
//                                  DatePicker.showSimpleDatePicker(
//                                    context,
//                                    initialDate: DateTime.now(),
//                                    firstDate: DateTime.now(),
//                                    lastDate: DateTime(2050),
//                                    dateFormat: "dd-MMMM-yyyy",
//                                    locale: DateTimePickerLocale.ru,
//                                    looping: true
//
//
//                                  )
//
//                                }
//
//                              ),
//                            ),
//                          ],
//                        ),
//                        Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: [
//                            Padding(
//                              padding: EdgeInsets.only(bottom: 10,left: 15),
//                              child: Text("Время",style: TextStyle(
//                                  color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
//                            ),
//                            Container(
//                              width: w/2-30,
//                              child: TextField(
//
//                                keyboardType: TextInputType.text,
//                                autofocus: false,
//                                decoration: InputDecoration(
//                                  hintText: '18:05',
//                                  hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
//                                  border: new OutlineInputBorder(
//                                      borderSide: BorderSide(
//                                          color: Color(0xffDFE0E3),
//                                      )
//                                  ),
//
//
//                                ),
//                                onTap:
//                                    ()=>{
//                                    DatePicker.showSimpleDatePicker(
//                                        context,
//                                        initialDate: DateTime.now(),
//                                        firstDate: DateTime.now(),
//                                        lastDate: DateTime(2050),
//
//                                    )
//
//                                }
//                              ),
//                            ),
//
//
//                          ],
//                        )
//
//                      ],
//                    ),

                  ],
                ),
              ),
            ),
            SizedBox(height: 30,),
            Container(
              width: w,
              color:Color(0xffF2FBFF),
              child: Padding(
                padding: const EdgeInsets.only(top:20.0,bottom: 34),
                child: Column(

                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [

                    SizedBox(
                      height: 28,
                    ),
                    Padding(

                      padding: const EdgeInsets.only(left:16.0,right: 16),
                      child: Text(
                        'Данные о товарах',
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.black,fontSize: 30),
                      ),
                    ),
                    SizedBox(height: 40,),
                    Column(children: productsWidget,),
                    SizedBox(height: 36,),
                    Padding(
                      key: dataKey,

                      padding: const EdgeInsets.symmetric(horizontal:16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: 10,left: 15),
                            child: Text(uniqueNameError != null ? uniqueNameError : "Наименование товара",style: TextStyle(
                                color:uniqueNameError != null ? Colors.red : Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                          ),
                          Container(

                            width: w-30,
                            child: Column(
                              children: [
                                TextField(
                                    controller: _productNameController,
                                    onSubmitted: (val){
                                      showProductDropdown = false;
                                      FocusScope.of(context).unfocus();

                                    },

                                    onEditingComplete: (){
                                      showProductDropdown = false;
                                      FocusScope.of(context).unfocus();

                                    },
                                    keyboardType: TextInputType.text,
                                    autofocus: false,

                                    onChanged: (String value){
                                      this.getProcessedProducts(value, 1);
                                      if(_productId == null && editableProduct == null){
                                        this.checkUnique();
                                      }

                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      hintText: 'Брюки',
                                      hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                                      border: new OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xffDFE0E3)
                                          )
                                      ),



                                    ),
                                  ),
                                Container(
                                  height: showProductDropdown == true ? 150 : 0,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color:Colors.grey,
                                        width: showProductDropdown == true  ? 2 : 0
                                    ),
                                    color: Colors.white
                                  ),
                                  child:loading != true ? ListView.builder(
                                    itemBuilder: (context,i){
                                    return
                                          GestureDetector(
                                            onTap: (){
                                              setState(() {
                                                uniqueNameError = null;
                                                uniqueArticulError = null;
                                                this.selectProduct(searchedProducts[i]);
                                              });
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(15),
                                              child: Text(searchedProducts[i]['title'],style: TextStyle(fontSize: 16),),
                                              decoration: BoxDecoration(
                                                border: Border(bottom: BorderSide(color: Colors.grey,width: 2))
                                              ),
                                            ),
                                          );
                                    },
                                    itemCount: searchedProducts.length,
                                  ) : Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                )
                              ],
                            ),
                            ),
                          Padding(
                            padding: EdgeInsets.only(top: 30,bottom: 10,left: 15),
                            child: Text(uniqueArticulError != null ? uniqueArticulError : "Артикул",style: TextStyle(
                                color: uniqueArticulError != null ? Colors.red : Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                          ),
                          Container(

                            width: w-30,
                            child: Column(
                              children: [
                                TextField(
                                  controller: _vendorCodeController,
                                  onChanged: (String value){

                                    this.getProcessedProducts(value, 0);
                                    if(_productId == null && editableProduct == null){
                                      this.checkUniqueArticul();
                                    }

                                  },
                                  focusNode: articleFocus,
                                  onSubmitted: (val){
                                    showVendorDropdown = false;
                                    FocusScope.of(context).unfocus();

                                  },
                                  onEditingComplete: (){
                                    showVendorDropdown = false;
                                    FocusScope.of(context).unfocus();

                                  },
                                  keyboardType: TextInputType.text,
                                  autofocus: false,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
//                                    hintText: '123e',
                                    hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                                    border: new OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey,

                                        )
                                    ),


                                  ),
                                ),
                                Container(
                                  height: showVendorDropdown == true ? 150 : 0,
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color:Colors.grey,
                                          width: showVendorDropdown == true  ? 2 : 0
                                      ),
                                      color: Colors.white
                                  ),
                                  child:loading != true ? ListView.builder(
                                    itemBuilder: (context,i){
                                      return
                                        GestureDetector(
                                          onTap: (){
                                            setState(() {
                                              uniqueNameError = null;
                                              uniqueArticulError = null;
                                              this.selectProduct(searchedProducts[i]);
                                            });
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.all(15),
                                            child: Text(searchedProducts[i]['articul'],style: TextStyle(fontSize: 16),),
                                            decoration: BoxDecoration(
                                                border: Border(bottom: BorderSide(color: Colors.grey,width: 2))
                                            ),
                                          ),
                                        );
                                    },
                                    itemCount: searchedProducts.length,
                                  ) : Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment:CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(top: 30,bottom: 10,left: 15),
                                    child: Text("Количество",style: TextStyle(
                                        color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                                  ),
                                  Container(
                                    width: w/2-30,
                                    child: TextField(
                                      controller: _quantityProduct,

                                      keyboardType: TextInputType.number,
                                      autofocus: false,
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
//                                        hintText: '12',
                                        hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                                        border: new OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.grey,

                                            )
                                        ),


                                      ),
                                      onChanged: (value){
                                        if(_priceController.text.isNotEmpty){
                                          setState(() {

                                            _sumController.text =  (int.parse(_priceController.text)*int.parse( _quantityProduct.text)) as String;

                                          });
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),

                              Column(
                                crossAxisAlignment:CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(top: 30,bottom: 10,left: 15),
                                    child: Text("Цена",style: TextStyle(
                                        color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                                  ),
                                  Container(
                                    width: w/2-30,
                                    child: TextField(
                                      controller: _priceController,

                                      keyboardType: TextInputType.number,
                                      autofocus: false,
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
//                                        hintText: '150',
                                        hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                                        border: new OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.grey,

                                            )
                                        ),



                                      ),

                                    ),
                                  ),
                                ],
                              )

                            ],
                          ),

                          Padding(
                            padding: EdgeInsets.only(top: 30),
                            child: GestureDetector(
                              onTap: (){


                                if(uniqueArticulError != null || uniqueNameError != null){
                                  return true;
                                }
                                audioPlay();

                                setState(() {
                                  if(editableProduct != null){
                                    products[int.parse(editableProduct)-1]['name'] = _productNameController.text;
                                    products[int.parse(editableProduct)-1]['articul'] = _vendorCodeController.text;
                                    products[int.parse(editableProduct)-1]['price'] = int.parse(_priceController.text);
                                    products[int.parse(editableProduct)-1]['amount'] = this.widget.isSaleInvoice == true ? -int.parse(_quantityProduct.text) : int.parse(_quantityProduct.text);

                                    products[int.parse(editableProduct)-1]['processed_product_id'] = processedProduct;
                                    productsWidget[int.parse(editableProduct)-1] = ProductCard(_productNameController.text, _vendorCodeController.text, (int.parse(editableProduct)).toString(), int.tryParse(_priceController.text) ,
                                        int.tryParse(_quantityProduct.text),
                                        deleteProduct, editProduct,editableProduct);
                                    _productNameController.text = '';
                                    _vendorCodeController.text = '';

                                    _priceController.text = '';
                                    _quantityProduct.text = '';
                                    editableProduct = null;
                                    processedProduct = null;
                                    var sum = 0;
                                     products.forEach((e) =>
                                     {
                                        sum +=
                                           e['price'] *
                                               e['amount'].abs()

                                     });
                                     _sumController.text = sum.toString();
                                     if(_paidSumController.text != ''){
                                       _debtController.text = (int.parse(_sumController.text)-int.parse(_paidSumController.text)).toString();

                                     }else{
                                       _debtController.text = int.parse(_sumController.text).toString();

                                     }

                                    SweetAlert.show(context,
                                        title: 'Успешно',
                                        subtitle: 'Товар был обновлен',
                                        style: SweetAlertStyle.success
                                    );

    

                                  }else{
                                    products.add({
                                      "title": _productNameController.text,
                                      "articul": _vendorCodeController.text,
                                      "price": int.parse(_priceController.text),
                                      "amount": this.widget.isSaleInvoice == true ? -int.parse(_quantityProduct.text) : int.parse(_quantityProduct.text),
                                      "id" :_productId == null ? null : _productId,
                                      "processed_product_id" :null

                                    });
                                    productsWidget.add(

                                        ProductCard(_productNameController.text, _vendorCodeController.text, (productsWidget.length+1).toString(), int.tryParse(_priceController.text) ,
                                            int.tryParse(_quantityProduct.text),
                                            deleteProduct, editProduct,null)
                                    );
                                    if(_sumController.text != ''){
                                      print(_sumController.text);
                                      _sumController.text = (int.parse(_sumController.text)+(int.parse(_priceController.text)*int.parse(_quantityProduct.text))).toString();
                                      print(_sumController.text);

                                    }else{
                                      _sumController.text  = ((int.parse(_priceController.text)*int.parse(_quantityProduct.text))).toString();

                                    }
                                    if(_debtController.text != ''){
                                      _debtController.text = (int.parse(_debtController.text)+((int.parse(_priceController.text)*int.parse(_quantityProduct.text)))).toString();

                                    }
                                    _productNameController.text = '';
                                    _vendorCodeController.text = '';
                                    _quantityProduct.text = '';
                                    _priceController.text = '';
                                    SweetAlert.show(context,
                                        title: 'Успешно',
                                        subtitle: 'Товар успешно добавлен',
                                        style: SweetAlertStyle.success
                                    );
                                  }
                                  print(products);

                                });
                              },
                              child: Center(
                                child: Text(
                                    '${editableProduct == null ? '+ Добавить еще товары' : 'Обновить товар' } ',
                                  style: TextStyle(fontSize: 18,color: Color(0xff1FBCFF),fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),

                    )

                  ],
                ),
              ),
            ),

            Container(
              width: w,
              color:Colors.white,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16,vertical: 44),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Итог',style: TextStyle(fontSize: 30),),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10,left: 15,top: 30),
                      child: Text("Итоговая сумма",style: TextStyle(
                          color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      width: w-30,
                      child: TextField(
                        enabled: false,
                        controller: _sumController,

                        keyboardType: TextInputType.number,
                        autofocus: false,
                        decoration: InputDecoration(
//                          hintText: '0',

                          hintStyle: TextStyle(fontSize: 18,color: Colors.black),
                          border: new OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xffDFE0E3)
                              )
                          ),



                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10,left: 15,top: 30),
                      child: Text("Оплачено",style: TextStyle(
                          color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      width: w-30,
                      child: TextField(
                        controller: _paidSumController,
                        keyboardType: TextInputType.number,
                        autofocus: false,
                        onChanged: (value){
                          setState(() {
                            if(this.widget.isSaleInvoice == true){
                              _debtController.text =( int.parse(_sumController.text) - int.parse(_paidSumController.text)).toString();

                            }else{
                              _debtController.text =( int.parse(_sumController.text) - int.parse(_paidSumController.text)).toString();

                            }
                          });
                        },
                        decoration: InputDecoration(
//                          hintText: '14550',

                          hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                          border: new OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xffDFE0E3)
                              )
                          ),



                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10,left: 15,top: 30),
                      child: Text("Долг",style: TextStyle(
                          color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      width: w-30,
                      child: TextField(
                        controller:_debtController,
                        keyboardType: TextInputType.number,
                        autofocus: false,
                        decoration: InputDecoration(
//                          hintText: '550',

                          hintStyle: TextStyle(fontSize: 18,color: Color(0xffBDC2CA)),
                          border: new OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xffDFE0E3)
                              )
                          ),



                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 32,
            ),
            GestureDetector(
              onTap: ()async{
                audioPlay();

                final prefs = await SharedPreferences.getInstance();
                List<String> errors = [];

                if(_partnerId == null){
                  errors.add('Выберите партнера');
                }

                if(products.length == 0){
                  errors.add('Выберите товары');
                }


                if(errors.length > 0){
                  SweetAlert.show(context,
                      title: 'Ошибка',
                      subtitle: errors.join('\n'),
                      style: SweetAlertStyle.error
                  );
                  return false;
                }

                final token = prefs.getString('token');
                setState(() {
                  onTapLoading = true;
                });
                if(this.widget.waybill != null){

                  final response = await http.put("$endpointApi/waybills/waybill/${this.widget.waybill['id']}",body: {
                    "partner_id": _partnerId,
                    "products" : jsonEncode(products),
                    "paid" : _paidSumController.text != '' ? this.widget.isSaleInvoice == true ? _paidSumController.text : ((-1)*int.parse(_paidSumController.text)).toString() : '0',
                    "total_sum" :this.widget.isSaleInvoice == true ?  _sumController.text : ((-1)*int.parse(_sumController.text)).toString(),
                  },headers: {
                    "Authorization" : "Bearer $token"
                  });



                  if(response.statusCode == 200) {
                    final jsonBody = jsonDecode(response.body);
                    setState(() {
                      onTapLoading = false;
                    });
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>SuccessPage(update: true, waybill: jsonBody['data'],)));
                  }
                }else{

                  final response = await http.post("$endpointApi/waybills",body: {
                    "partner_id": _partnerId,
                    "products" : jsonEncode(products),
                    "paid" : _paidSumController.text != '' ? this.widget.isSaleInvoice == true ? _paidSumController.text : (-int.parse(_paidSumController.text)).toString() : '0',
                    "total_sum" :this.widget.isSaleInvoice == true ?  _sumController.text : (-int.parse(_sumController.text)).toString(),
                  },headers: {
                    "Authorization" : "Bearer $token"
                  });
                  print(response.body);
                  if(response.statusCode == 200) {
                    final jsonBody = jsonDecode(response.body);
                    setState(() {
                      onTapLoading = false;
                    });
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>SuccessPage(update: false,waybill: jsonBody['data'],)));
                  }
                }



              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal:16.0),
                child: Container(
                  width: w,
                  height: 70,

                  decoration: BoxDecoration(
                    color: Color(0xff1FBCFF),
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Center(
                      child:
                      onTapLoading == false ? Text('${this.widget.waybill != null ? 'Обновить накладную' : 'Создать накладную'}',
                        style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500,color:Colors.white),
                      ): CircularProgressIndicator(
                        backgroundColor: Colors.white,

                      )
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                audioPlay();

                Navigator.pop(context);
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical:20),
                child: Center(
                  child: Text('Отменить',style: TextStyle(fontSize: 14,fontWeight: FontWeight.w500,color:Theme.of(context).primaryColor),),
                ),
              ),
            ),

          ],
        ),

      ),
    );
  }
}



class ProductCard extends StatelessWidget {
  String name;
  String vendorCode;
  String numeration;
  final int quantity;
  final int price;
  final deleteProduct;
  Function editProduct;
  final productId;

  ProductCard(this.name,this.vendorCode,this.numeration,this.price,this.quantity,this.deleteProduct,this.editProduct, this.productId);

  InvoiceCreatePage parent;


  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return  new InkResponse(
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(top: 15,bottom: 20,left: 16,right: 16),
          child: Column(
            children: [
              Row(
                children: [
                  Text('$numeration',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
                  SizedBox(width: 20,),
                  Text('${name.length <10 ? name: name.substring(0,10)}',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
                  SizedBox(width: 135),
                  Text('Артикул ${vendorCode.length < 5 ? vendorCode : vendorCode.substring(0,5) }',style: TextStyle(fontSize: 16,color: Color(0xffC2CACD)),)
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('${quantity.abs()} шт',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),),
                  Text('$price ₸',style: TextStyle(fontSize: 16,)),
                  Text('${quantity.abs()*price} ₸',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),)
                ],
              ),
              SizedBox(height: 12,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap:(){
                      audioPlay();

                      this.editProduct(name,price,vendorCode,quantity,numeration,productId);
                    },
                    child: Container(
                      width: w/2 - 34,
                      height: 36,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color:  Theme.of(context).primaryColor.withOpacity(1),
                              // changes position of shadow
                              spreadRadius: 1,

                            ),
                          ]
                      ),
                      child: Center(
                        child: Text('Редактировать',style: TextStyle(
                            fontSize: 16,
                            color: Colors.black
                        ),),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap:(){
                      audioPlay();

                      this.deleteProduct(this.numeration);
                    },
                    child: Container(
                      width: w/2 - 34,
                      height: 36,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color:  Theme.of(context).secondaryHeaderColor.withOpacity(1),
                              // changes position of shadow
                              spreadRadius: 1,

                            ),
                          ]
                      ),
                      child: Center(
                          child: Text('Удалить',style: TextStyle(
                              fontSize: 16,
                              color: Color(0xffBDC2CA)
                          ),),
                        ),
                      ),
                  ),

                ],
              ),

            ],
          ),

        ),
        decoration: BoxDecoration(
            color: Colors.white
        ),

      ),
    );
  }
}
