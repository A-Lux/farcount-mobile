import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:farcount/Printing/Bluetooth.dart';
import 'package:farcount/Printing/PrintingIos.dart';
import 'package:farcount/Setting.dart';
import 'package:farcount/Widgets/BlueActionButton.dart';
import 'package:farcount/pdf/InvoicePage.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/views/Sales/CreateInvoice.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:image/image.dart' as dart_image;

import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:native_pdf_renderer/native_pdf_renderer.dart' as PdfRenderer;

class OwePersonView extends StatefulWidget {
  static var id = 'owepersonview';
  final waybill;

  OwePersonView({Key key, this.waybill}) : super(key: key);
  @override
  _OwePersonViewState createState() => _OwePersonViewState();
}

class _OwePersonViewState extends State<OwePersonView> {
  Map<String,dynamic> debtor;
  List<dynamic> productWidgets;
  List<pw.TableRow> pdfWidgets = [];
  bool loading;
  showDeleteDialog(BuildContext context,text){
    Widget okButton = FlatButton(
      child: Text("Удалить"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("$text"),

      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  Future<dynamic> deleteWaybill(waybillId) async{
    final prefs = await SharedPreferences.getInstance();

    final token = prefs.getString('token');
    var res = await http.delete('$endpointApi/waybills/waybill/$waybillId',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"

    },);

    print(res.body);

    return true;
  }
  Future<dynamic> getDebtor()async{
    setState(() {
      loading = true;
    });
    final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
    final ttf = pw.Font.ttf(font);
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    var res = await http.get('${endpointApi}/waybills/show/${widget.waybill}',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"

    },);
    var w = MediaQuery.of(context).size.width;

    var responseBody = jsonDecode(res.body)['data'];
    print(responseBody);

    setState(() {

      productWidgets = responseBody['products'];

      debtor = responseBody;

      loading = false;




    });

    return 'success';
  }
  
  @override
  void initState() {
    // TODO: implement initState
    print(widget.waybill);
    super.initState();
    this.getDebtor();
//    if(this.widget.waybill['total_sum'] is String){
//      print(this.widget.waybill['total_sum'].runtimeType);
//
//      this.widget.waybill['total_sum'] = int.parse(this.widget.waybill['total_sum']);
//      this.widget.waybill['paid'] = int.parse(this.widget.waybill['paid'].toString());
//
//    }
  }
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color(0xffffffff),
      appBar: AppBar(title: Text(
          'Накладная #${debtor != null ? debtor['id'] : ''}'
      ),
        elevation: 1,
        centerTitle: true,),
      body: RefreshIndicator(
        key:refreshKey,
        onRefresh: ()async{
          this.getDebtor();

        },
        child: debtor != null  ?
          Stack(
            children: [
              SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(color: Color(0xffC2CACD))),
                              color: Colors.white
                          ),
                          width: w,
                          height: 125,
                          padding: EdgeInsets.symmetric(horizontal: 32,vertical: 16),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    height: 73,
                                    width: (w/3*2)-64,
                                    child: Text(
                                      '${this.debtor != null ? this.debtor['partner']['name'] : ''}',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,

                                      ),
                                    ),
                                  ),
                                  Container(
                                      height: 60,
                                      width: w-((w/3*2)),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          GestureDetector(
                                            onTap: (){
                                              audioPlay();
                                              Navigator.push(context, MaterialPageRoute(builder: (context)=>InvoiceCreatePage(waybill: this.debtor,isSaleInvoice:this.debtor['total_sum'] > 0 ? true :false,)));
                                            },
                                            child: Icon(Icons.edit,size: 30,color: Color(0xffC2CACD),),

                                          ),
                                          SizedBox(width: 24,),
                                          GestureDetector(
                                            onTap: (){
                                              audioPlay();

                                              if(debtor['total_sum'] is String ){
                                                print(debtor['total_sum'] is int);
                                                debtor['total_sum'] = int.parse(debtor['total_sum']);
                                                debtor['paid'] = int.parse(debtor['paid']);
                                              }
                                              SweetAlert.show(
                                                  context,
                                                  subtitle: 'Вы точно хотите удалить ?',
                                                  style: SweetAlertStyle.confirm,
                                                  showCancelButton: true,
                                                  cancelButtonText: 'Отменить',
                                                  confirmButtonText: 'Подтвердить',
                                                  onPress: (bool isConfirm){
                                                    if(isConfirm){
                                                      this.getDebtor();

                                                      this.deleteWaybill(debtor['id']);
                                                      SweetAlert.show(context,style: SweetAlertStyle.success,title: "Накладная удалена");
                                                      Navigator.pop(context);



                                                    }else{
                                                      return true;
                                                    }
                                                    return false;
                                                  }

                                              );
                                            },
                                            child: Icon(Icons.delete,size: 30,color: Color(0xffC2CACD),),
                                          )
                                        ],
                                      )
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text('#${debtor != null ? debtor['id'] : ''}',style: TextStyle(
                                    color: Color(0xffC2CACD),
                                    fontSize: 16,
                                  ),),
                                  SizedBox(width: 22,),
                                  Text('${DateTime.parse(debtor['created_at']).day}.${DateTime.parse(debtor['created_at']).month}.${DateTime.parse(debtor['created_at']).year}',style: TextStyle(
                                    fontSize: 16,
                                  ),),
                                  SizedBox(width: 22,),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(100),
                                        color: debtor['total_sum'] - debtor['paid'] != 0 ? Colors.red : Colors.green
                                    ),
                                    height: 7,
                                    width: 7,
                                  ),
                                  SizedBox(width: 8,),
                                  Text('${debtor['total_sum'] - debtor['paid'] != 0 ? 'Долг' : 'Оплачено'}',style: TextStyle(
                                    fontSize: 16,
                                  ),),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: w,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color:Color.fromRGBO(117, 120, 123, 0.15),
                                  spreadRadius: 2,
                                  blurRadius: 8,
                                  offset: Offset(0, 2), // changes position of shadow
                                ),
                              ]
                          ),
                          height: 100,
                          padding: EdgeInsets.symmetric(horizontal: 32,vertical: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ShortInfo(heading: 'Сумма итого',seconf: '${debtor != null ? debtor['total_sum'].abs() : ''} ₸',),
                              ShortInfo(heading: 'Оплачено',seconf: '${debtor != null ? debtor['paid'].abs() : ''} ₸',),
                              ShortInfo(heading: 'Долг',seconf: '${debtor != null ? debtor['total_sum'].abs()-debtor['paid'].abs() : ''} ₸',)

                            ],
                          ),
                        ),
                        Container(
                          color: Color(0xffF2FBFF),
                          width: w,
                          height: 80,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 26,horizontal: 32),
                            child: Text('Данные о товарах',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 24,
                              ),),
                          ),
                        ),
                        Container(

                          width: w,
                          child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context,i){
                            return InfoTile(isLast: i == 2,i: i,color: 0xffF2FBFF,product: productWidgets!= null ? productWidgets[i] : null,);
                          },itemCount: productWidgets != null ? productWidgets.length : 3,),
                        ),
                        SizedBox(height: 16,),
                        GestureDetector(
                          onTap:() async{
                            final doc = pw.Document();




//                      Navigator.push(context, MaterialPageRoute(builder: (context)=>());
                            final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
                            final ttf = pw.Font.ttf(font);
                            pdfWidgets = [];
                            pdfWidgets.add(
                                pw.TableRow(
                                    children: [
                                      pw.Text('Артикул',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                      pw.Text('Название',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                      pw.Text('Количество',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                      pw.Text('Цена за штуку',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                      pw.Text('Сумма',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                    ]
                                )
                            );
                            productWidgets.forEach((element) {
                              pdfWidgets.add(pw.TableRow(
                                  children: [
                                    pw.Text('${element['articul'].length > 10 ? element['articul'].substring(0,10) : element['articul']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                    pw.Text('${element['title'].length > 10 ? element['title'].substring(0,10) : element['title']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                    pw.Text('${element['amount'].abs()}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                    pw.Text('${element['price'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                    pw.Text('${element['amount'].abs()*element['price'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                  ]
                              ));
                            });
                            doc.addPage(
                              pw.Page(
                                  build: (pw.Context context) =>pw.Column(
                                      children: [
                                        pw.Table(
                                            border: pw.TableBorder(),
                                            children: [
                                              pw.TableRow(
                                                  children: [
                                                    pw.Text('Накладная ${debtor['id']}',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                                  ]
                                              ),
                                              pw.TableRow(
                                                  children: [
                                                    pw.Text('Общая сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                                    pw.Text('${debtor['total_sum'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                                  ]
                                              ),
                                              pw.TableRow(
                                                  children: [
                                                    pw.Text('Партнер ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                                    pw.Text('${debtor['partner']['name']} ',style: pw.TextStyle(font: ttf,fontSize: 16)),


                                                  ]
                                              ),
                                              pw.TableRow(
                                                  children: [
                                                    pw.Text('Оплачено ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                                    pw.Text('${debtor['paid'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                                  ]
                                              ),
                                              pw.TableRow(
                                                  children: [
                                                    pw.Text('Осталось ',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                                    pw.Text(' ${debtor['total_sum'].abs()-debtor['paid'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                                  ]
                                              ),



                                            ]
                                        ),
                                        pw.Table(
                                            border: pw.TableBorder(),
                                            children: pdfWidgets
                                        )
                                      ]
                                  )
                              ),
                            );
                            audioPlay();


                            await Printing.sharePdf(bytes: doc.save(), filename: 'my-document.pdf');

                          },
                          child: Container(
                            width: w-32,
                            height: 70,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                              color: Color(0xff2BC143)
                            ),
                            child: Center(
                              child: Text('Поделиться в Whatsapp',style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white
                              ),),
                            ),
                          ),
                        ),
                        SizedBox(height: 150,),

                      ],
                    ),
                  ),
            ],
          ): Center(child: CircularProgressIndicator(),)

      ),
      bottomNavigationBar:
        Container(
          width: w,
          height: 85,
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color:Color.fromRGBO(117, 120, 123, 0.15),
                  spreadRadius: 8,
                  blurRadius: 10,
                  offset: Offset(0, -10), // changes position of shadow
                ),
              ]
          ),
          padding: EdgeInsets.only(left: 16,right: 16,top: 16,bottom: 16),
          child: GestureDetector(

            child: BlueButton(text: 'Распечатать накладную',),
            onTap: () async{
              await audioPlay();


                Navigator.push(context, MaterialPageRoute(
                    builder: (context) =>
                        BluetoothIos(waybill: debtor,
                            products: productWidgets,
                         )));




            },
          ),
        ),


    );
  }
}
class InfoTile extends StatelessWidget {
  final isLast;
  final i;
  int color = 0xffF2FBFF;
  Map<String,dynamic> product;
  InfoTile({this.isLast,this.i,this.color,this.product});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Container(
      width: w,
      height: 80,
      decoration: BoxDecoration(
          color: Color(this.color),
          border:isLast ? Border(top: BorderSide(color: Color(0xff8EB2C2)),bottom: BorderSide(color: Color(0xff8EB2C2))) : Border(top: BorderSide(color: Color(0xff8EB2C2)))
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 32,right: 32,top: 8),
        child:product != null ? Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text((i+1).toString() + '     ' + '${product['title'].length <20 ? product['title']:product['title'].substring(0,20)}',style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold
                ),),
                Text('Артикул: ${product['articul'].length <5 ?product['articul'] : product['articul'].substring(0,5)}',style: TextStyle(
                  color: Color(0xffC2CACD),
                  fontSize: 16,
                ),),

              ],
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('      ' + '${product['amount'].abs()} шт',style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                ),),
                Container(

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('${product['price'].abs()} ₸',style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),),
                      SizedBox(width: 20,),
                      Text('${(product['price'].abs()*product['amount'].abs()).toString()} ₸',style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),),
                    ],
                  ),
                ),

              ],
            ),



          ],
        ):Center(child: Text(
          'Нет продуктов'
        ),),
      ),
    );
  }
}


class ShortInfo extends StatelessWidget {
  final heading;
  final seconf;
  Map<String,dynamic> product;
  ShortInfo({this.heading,this.seconf,this.product});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(heading,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 16
          ),),
          SizedBox(height: 8,),
          Text(seconf,
            style: TextStyle(
                color: Colors.black,
                fontSize: 16
            ),)
        ],
      ),
    );
  }
}
