import 'dart:convert';

import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/bottomNabBar.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../Setting.dart';

class OweList extends StatefulWidget {
  static var id = 'owelist';
  final int debtProp;

  const OweList({Key key, this.debtProp}) : super(key: key);

  @override
  _OweListState createState() => _OweListState();
}

class _OweListState extends State<OweList> {
  List<dynamic> debtors;
  List<Widget> debtorsWidget;
  String sortType = 'total_sum';
  String sortValue = 'desc';
  int debt = 0;
  bool loading = false;
  TextEditingController _searchController = TextEditingController();
  Future<dynamic> getDebtors(sortType,sortValue)async{
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print(this.widget.debtProp);
    var res = await http.get('${endpointApi}/waybills?not_paid=1&sort_type=${sortType}&sort_value=${sortValue}&is_debt=${this.widget.debtProp == null ? '0': '1'}&${_searchController.text != '' ? 'term=${_searchController.text}' : ''}',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"

    },);

    print(token);
    var w = MediaQuery.of(context).size.width;
    print(res.body);
    var responseBody = jsonDecode(res.body)['data'];
    setState(() {
      loading = false;
      debtors = responseBody;
      var i = 1;


    });

    return 'success';
  }
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      if(this.widget.debtProp == 1){
        debt = this.widget.debtProp;
      }
      this.getDebtors('total_sum','desc');
  }
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xffffffff),
      appBar: AppBar(title: Text(
        'Список ${this.widget.debtProp == null ? 'должников' : 'кредиторов'}'
      ),
      elevation: 1,
      centerTitle: true,),
      body: RefreshIndicator(
        onRefresh: ()async{
          this.getDebtors(sortType, sortValue);
        },
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),


          key: refreshKey,
          child: Container(
            width: w,
            child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Container(
                    child: Container(
                      width: w -32,
                      height: 70,
                      child: TextField(

                        controller: _searchController,
                        cursorColor: Colors.black,
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                            borderRadius: new BorderRadius.circular(8.0),
                          ),
                          hintText: 'Поиск по накладным',
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                            borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                          ),
                          suffixIcon: Icon(Icons.search,color: Color(0xffDFE0E3)),
                        ),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          color: Colors.black,
                        ),
                        onChanged: (val){
                          this.getDebtors(sortType, sortValue);

                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text('Сортировать по:',style: TextStyle(
                   color: Color(0xffC2CACD),
                   fontSize: 14,
               ),),
                ),
                SizedBox(height: 8,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(children: [
                    GestureDetector(
                      onTap:(){
                        audioPlay();

                        setState(() {
                          if(sortType == 'total_sum'){
                            sortValue == 'desc' ? sortValue = 'asc' : sortValue = 'desc';
                          }else{
                            sortValue = 'desc';

                            sortType  = 'total_sum';

                          }


                          this.getDebtors(sortType, sortValue);
                        });


                      },
                      child: Container(
                        width: w/2 - 24,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border:  Border.all(color: Theme.of(context).primaryColor),

                            color: sortType == 'total_sum' ? Theme.of(context).primaryColor : Colors.white,

                            boxShadow: [
                              BoxShadow(
                                color:  Theme.of(context).primaryColor.withOpacity(0.5),
                                spreadRadius:sortType == 'total_sum' ? 2 :0,
                                blurRadius: sortType == 'total_sum' ? 8 :0,
                                offset: sortType == 'total_sum' ? Offset(0, 4) : Offset(0, 0), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment:sortType == 'total_sum' ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.center,
                            children: [

                              Text('Cумма долга',style: TextStyle(
                                  fontSize: 16,
                                  color: sortType == 'total_sum' ? Colors.white :Colors.black
                              ),),
                              sortType == 'total_sum' ? Icon(sortValue == 'desc' ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up,color: Colors.white,) :
                                  SizedBox.shrink()
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 16,),
                    GestureDetector(
                      onTap: (){
                        audioPlay();

                        setState(() {
                          if(sortType == 'name'){
                            sortValue == 'desc' ? sortValue = 'asc' : sortValue='desc';

                          }else{
                            sortValue = 'desc';
                            sortType = 'name';
                          }
                          this.getDebtors(sortType, sortValue);
                        });
                      },
                      child: Container(
                        width: w/2 - 24,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: Theme.of(context).primaryColor),
                            color:  sortType == 'name' ? Theme.of(context).primaryColor :Color(0xffffffff),
                            boxShadow: [
                              BoxShadow(
                                color:  Theme.of(context).primaryColor.withOpacity(0.5),
                                spreadRadius:sortType == 'name' ? 2:0,
                                blurRadius: sortType == 'name' ? 8:0,
                                offset:sortType == 'name' ?Offset(0, 5) : Offset(0, 0), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment:sortType == 'name' ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.center ,
                            children: [
                              Text('Имени',style: TextStyle(
                                fontSize: 16,
                                color: sortType == 'name' ? Colors.white : Colors.black,
                            ),),
                              sortType == 'name' ? Icon(sortValue == 'desc' ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up,color: Colors.white,):
                                  SizedBox.shrink()
                            ]
                          ),
                        ),
                      ),
                    ),
                  ],),
                ),
                SizedBox(height: 16,),
                Container(
                    width: w,
                    child:
                        loading == true ?
                        Container(
                          padding: const EdgeInsets.all(100),
                          child: Center(child: CircularProgressIndicator()),
                        ) :
                        Column(
                      children: <Widget>[
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),

                          itemBuilder: (context,i){
                            if(debtors.length != 0){
                              return GestureDetector(
                                onTap: (){

                                  audioPlay();
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> OwePersonView(waybill:debtors[i]['id'])));
                                },
                                child: Container(
                                  height: 80,
                                  width: w,
                                  decoration: BoxDecoration(
                                    border: i == 2 ? Border.symmetric(vertical: BorderSide(color:Color(0xffDFE0E3)))  : Border(top: BorderSide(color: Color(0xffDFE0E3),)),
                                  ),
                                  padding: EdgeInsets.symmetric(horizontal: 16,vertical: 8),
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Text('#${debtors[i]['id']}',style: TextStyle(
                                            color: Color(0xffC2CACD),
                                            fontSize: 12,
                                          ),),
                                          SizedBox(width: 16,),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text('${debtors[i]['partner_id']}',style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18,
                                              ),),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  PriceContainer(debtors[i]['total_sum'],debtors[i]['paid']),
                                                  SizedBox(width: 16,),
                                                  Text(
                                                      '${debtors != null ?
                                                      '${DateTime.parse(debtors[i]['created_at']).day}.${DateTime.parse(debtors[i]['created_at']).month}.${DateTime.parse(debtors[i]['created_at']
                                                      ).year}' : null} '),

                                                ],
                                              )
                                            ],
                                          )
                                        ],

                                      ),
                                      Positioned(
                                          right:100,
                                          child:Padding(
                                            padding: const EdgeInsets.only(bottom:20.0),
                                            child: Text('.',style: TextStyle(fontSize: 50,color:debtors != null && debtors[i]['total_sum']-debtors[i]['paid'] != 0 ?  Colors.red : Colors.green),),
                                          )
                                      ),
                                      Positioned(
                                        right: 0,
                                        child: GestureDetector(
                                            onTap: (){
                                              audioPlay();

                                              Navigator.push(context, MaterialPageRoute(builder: (context)=> OwePersonView(waybill:debtors[i]['id'])));
                                            },
                                            child: Icon(Icons.navigate_next,color: Color(0xffC2CACD),size: 40,)),
                                      )
                                    ],
                                  ),
                                ),
                              );

                            }else{
                              return Center(child: Text('Пока что нет должников'),);
                            }
                          },itemCount: debtors != null ?debtors.length:0,)
                      ],
                    ),
                  ),

              ],
            ),
          ),
        ),
      ),

    );
  }
}
class PriceContainer extends StatelessWidget {
  int sum;
  int debt;

  PriceContainer(this.sum,this.debt);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Color(0xffEBF9FF),
      ),
      child: Center(child:
      Text(
        '${(this.sum-this.debt).abs().toString()} ₸',style: TextStyle(
          color: Theme.of(context).primaryColor,
          fontSize: 14
      ),
      ),),
    );
  }
}
