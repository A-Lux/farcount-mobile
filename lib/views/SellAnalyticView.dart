import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/material.dart';

class SaleAnalyticScene extends StatelessWidget {
  static var id = 'sellanalytic';
  var entries = [1, 2, 3,];
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xffffffff),
      appBar: AppBar(
        title: Text('Аналитка продаж'),
        elevation: 1,
        centerTitle: true,
      ),
      body: Container(
        width: w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Выберите период:',
                    style: TextStyle(color: Color(0xffC2CACD), fontSize: 16),
                  ),
                  Row(
                    children: [
                      Text(
                        '1 — 31 сентября',
                        style:
                            TextStyle(color: Color(0xffC2CACD), fontSize: 16),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Icon(
                        Icons.calendar_today,
                        color: Color(0xffC2CACD),
                        size: 20,
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 16,
            ),
            SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: [
                  Container(
                    width: (w - 32) / 3 - 6.3,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Theme.of(context).primaryColor,
                        boxShadow: [
                          BoxShadow(
                            color:
                                Theme.of(context).primaryColor.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 8,
                            offset: Offset(0, 4), // changes position of shadow
                          ),
                        ]),
                    child: Center(
                      child: Text(
                        'по клиентам',
                        style: TextStyle(fontSize: 14, color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Container(
                    width: (w - 32) / 3 - 6.3,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border:
                            Border.all(color: Theme.of(context).primaryColor)),
                    child: Center(
                      child: Text(
                        'по товарам',
                        style: TextStyle(fontSize: 13, color: Colors.black),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Container(
                    width: (w - 32) / 3 - 6.3,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border:
                            Border.all(color: Theme.of(context).primaryColor)),
                    child: Center(
                      child: Text(
                        'по товарам',
                        style: TextStyle(fontSize: 13, color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 24,
            ),
            Expanded(
              child: Container(
                width: w,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(height: 8,),
                        Container(
                          width: w-32,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(117, 120, 123, 0.15),
                                  spreadRadius: 5,
                                  blurRadius: 8,
                                  offset: Offset(0, 0), // changes position of shadow
                                ),
                              ]),
                          child: Column(
                            children: [
                              ...entries.map<Widget>(
                                    (i) => ListTileInfo(i:i),
                              ).followedBy([Container(
                                  height: 80,
                                  width: w-32,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8),bottomRight: Radius.circular(8))
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Раскрыть список',
                                      style: TextStyle(
                                          color: Color(0xffC2CACD),
                                          fontSize: 18),
                                    ),
                                  )
                              )]),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Container(
                          width: w,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Маржа за период',
                                style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Выберите период:',
                                      style: TextStyle(
                                          color: Color(0xffC2CACD),
                                          fontSize: 16),
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          '1 — 31 сентября',
                                          style: TextStyle(
                                              color: Color(0xffC2CACD),
                                              fontSize: 16),
                                        ),
                                        SizedBox(
                                          width: 16,
                                        ),
                                        Icon(
                                          Icons.calendar_today,
                                          color: Color(0xffC2CACD),
                                          size: 20,
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16),
                                child: Container(
                                  width: w,
                                  height: 65,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(
                                              117, 120, 123, 0.15),
                                          spreadRadius: 2,
                                          blurRadius: 8,
                                          offset: Offset(0,
                                              4), // changes position of shadow
                                        ),
                                      ]),
                                  child: Center(
                                    child: Text(
                                      '560 230 ₸',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 64,
                        )
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}

class PriceContainer extends StatelessWidget {
  final isFirst;
  final i;
  PriceContainer({this.isFirst,this.i});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: (isFirst ?? false) ? Colors.white : Color(0xffEBF9FF),
      ),
      child: Center(
        child: Text(
          '3500 ₸',
          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14),
        ),
      ),
    );
  }
}
class ListTileInfo extends StatelessWidget {
  final i;
  ListTileInfo({this.i});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Container(
      height: 80,
      width: w,
      decoration: BoxDecoration(
          border:  i == 1
              ? Border.all(color: Color(0xffF2FBFF))
              : Border(
              bottom: BorderSide(
                color: Color(0xffDFE0E3),
              )),
          color:
          i == 1 ? Color(0xffF2FBFF) : Colors.white,
          borderRadius: i == 1
              ? BorderRadius.circular(16)
              : null),
      padding: EdgeInsets.symmetric(
          horizontal: 16, vertical: 8),
      child:
          Stack(
        alignment: Alignment.center,
        children: [
          Row(
            crossAxisAlignment:
            CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5,left: 16,right: 24),
                child: Text(
                  i.toString(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Александров Николай',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    PriceContainer(isFirst: i == 1,),
                  ],
                ),
              )
            ],
          ),
          Positioned(
            right: 0,
            child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                      context, OwePersonView.id);
                },
                child: Icon(
                  Icons.navigate_next,
                  color: Color(0xffC2CACD),
                  size: 40,
                )),
          )
        ],
      ),
    );
  }
}
