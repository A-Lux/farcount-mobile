
import 'dart:io';

import 'package:farcount/AppViewController.dart';
import 'package:farcount/Pages/MainPage.dart';
import 'package:farcount/Pages/Sells.dart';
import 'package:farcount/Printing/Bluetooth.dart';
import 'package:farcount/Printing/PrintingIos.dart';
import 'package:farcount/views/SellAnalyticView.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pdf/pdf.dart';

import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:sweetalert/sweetalert.dart';
class SuccessPage extends StatefulWidget {
  static var id = 'success';
  final bool update;
  final waybill;

  const SuccessPage({Key key, this.update, this.waybill}) : super(key: key);
  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.waybill);
  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Создать накладную'),

      ),
      body: SingleChildScrollView(

        child: Container(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16,vertical: 50),

            child: Column(
              children: [
                  Center(
                    child: Image.asset('assets/correct.png',width: 106,height: 106,),
                  ),
                  SizedBox(
                    height: 26,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Text(
                          '${this.widget.update == false ? 'Накладная создана\n успешно': 'Накладная обновлена\n успешно'}',
                          style: TextStyle(

                              fontSize: 30,
                              fontWeight: FontWeight.w400,

                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 50),
                    child: ButtonTheme(
                      minWidth: w-30,
                      height: 70,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: Theme.of(context).primaryColor,

                        ),
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: RaisedButton(

                        onPressed: () async {

                            Navigator.push(context, MaterialPageRoute(builder: (context)=>BluetoothIos(waybill: this.widget.waybill['waybill'],products: this.widget.waybill['products'],)));




                          return true;


                        },
                        color: Theme.of(context).primaryColor,
                        child: Text('Распечатать накладную',
                          style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.w500),),


                      ),
                    ),

                  ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: ButtonTheme(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Color(0xff2BC143)
                        ),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    minWidth: w-30,
                    height: 70,
                    child: RaisedButton(

                      onPressed:() async{
                        final doc = pw.Document();




//                      Navigator.push(context, MaterialPageRoute(builder: (context)=>());
                        final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
                        final ttf = pw.Font.ttf(font);
                        List<pw.TableRow> pdfWidgets = [];
                        pdfWidgets.add(
                            pw.TableRow(
                                children: [
                                  pw.Text('Артикул',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                  pw.Text('Название',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                  pw.Text('Количество',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                  pw.Text('Цена за штуку',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                  pw.Text('Сумма',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                ]
                            )
                        );
                        widget.waybill['products'].forEach((element) {
                          pdfWidgets.add(pw.TableRow(
                              children: [
                                pw.Text('${element['articul'].length > 10 ? element['articul'].substring(0,10) : element['articul']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                pw.Text('${element['title'].length > 10 ? element['title'].substring(0,10) : element['title']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                pw.Text('${element['price'].abs()}',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                pw.Text('${element['amount'].abs()}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                pw.Text('${element['amount'].abs()*element['price'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),

                              ]
                          ));
                        });
                        final debtor = widget.waybill['waybill'];
                        doc.addPage(
                          pw.Page(
                              build: (pw.Context context) =>pw.Column(
                                  children: [
                                    pw.Table(
                                        border: pw.TableBorder(),
                                        children: [
                                          pw.TableRow(
                                              children: [
                                                pw.Text('Накладная ${debtor['id']}',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                              ]
                                          ),
                                          pw.TableRow(
                                              children: [
                                                pw.Text('Общая сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                                pw.Text('${int.parse(debtor['total_sum']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                              ]
                                          ),
                                          pw.TableRow(
                                              children: [
                                                pw.Text('Партнер ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                                pw.Text('${widget.waybill['partner']['name']}',style: pw.TextStyle(font: ttf,fontSize: 16)),


                                              ]
                                          ),
                                          pw.TableRow(
                                              children: [
                                                pw.Text('Оплачено ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                                pw.Text('${int.parse(debtor['paid']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                              ]
                                          ),
                                          pw.TableRow(
                                              children: [
                                                pw.Text('Осталось ',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                                pw.Text(' ${int.parse(debtor['total_sum']).abs()-int.parse(debtor['paid']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                              ]
                                          ),



                                        ]
                                    ),
                                    pw.Table(
                                        border: pw.TableBorder(),
                                        children: pdfWidgets
                                    )
                                  ]
                              )
                          ),
                        );



                        await Printing.sharePdf(bytes: doc.save(), filename: 'my-document.pdf');

                      },
                      color: Color(0xff2BC143),

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset('assets/whatsapp.png'),
                          Text('Поделиться в Whatsapp',
                            style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.w500),),
                        ],
                      ),




                    ),
                  ),

                ),
                GestureDetector(
                  onTap: ()async{


                    var count = 0;
                    Navigator.popUntil(context,(route){
                      return count ++ == 2;
                    });
                  },
                  child: Padding(
                      padding: EdgeInsets.only(top: 32),
                      child: Text('Вернуться в главное меню',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500,color: Color(0xffBDC2CA)),),
                  ),
                )

              ],
            ),
          ),
        ),
      ),
    );
  }
}
