import 'dart:io';

import 'package:farcount/provider/AuthProvider.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HelpPage extends StatefulWidget {
  final title;
  final url;

  const HelpPage({Key key, this.title, this.url}) : super(key: key);

  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('${this.widget.title}'),centerTitle: true,),
      body: WebView(
        initialUrl: '$endpointSite${this.widget.url}',
      ),
    );
  }
}
