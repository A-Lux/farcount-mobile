import 'dart:convert';

import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

import '../../Setting.dart';

class TopSalesPage extends StatefulWidget {
  static var id = 'top_sales';

  @override
  _TopSalesPageState createState() => _TopSalesPageState();
}

class _TopSalesPageState extends State<TopSalesPage> {
  String sortType = 'name';
  List<dynamic> debtors = [];
  bool loading = false;
  DateTime dateInitial = DateTime.now().subtract(new Duration(days:30));
  DateTime lastDate = DateTime.now();

  List<String> months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];

  Future<dynamic> getDebtors([List<dynamic> date])async{
    setState(() {
      loading = true;

    });

    final prefs = await SharedPreferences.getInstance();

    final token = prefs.getString('token');

    if(sortType =='name'){
      print(sortType);
      var res = await http.get('${endpointApi}/partners/debtors?sort_type=debt&sort_value=desc',headers: {
        "Accept" : "Application/json",
        "Authorization" : "Bearer $token"

      },);
      if(date != null){

        var res = await http.get(
          '$endpointApi/partners/debtors?sort_type=debt&sort_value=desc&date[]=${date[0][0]}&date=${date[0][1]}',
          headers: {
            "Accept" : "Application/json",
            "Authorization" : "Bearer $token"
          },
        );
      }
      var w = MediaQuery.of(context).size.width;
      print(res.body.toString());
      var responseBody = jsonDecode(res.body);
      print(responseBody);
      setState(() {
        loading = false;

        if(responseBody.length > 0){
          debtors = responseBody;

        }else{
          debtors = null;
        }
        var i = 1;


      });
    }

    if(sortType == 'total_sum'){

      var res = await http.get('${endpointApi}/waybills?sort_type=${sortType}&sort_value=desc',headers: {
        "Accept" : "Application/json",
        "Authorization" : "Bearer $token"
      },);
      if(date != null){
        var res = await http.get(
          '$endpointApi/partners/debtors?sort_type=debt&sort_value=desc&date[]=${date[0][0]}&date=${date[0][1]}',
          headers: {
            "Accept" : "Application/json",
            "Authorization" : "Bearer $token"
          },
        );
      }
      var w = MediaQuery.of(context).size.width;
      print(res.body);
      var responseBody = jsonDecode(res.body)['data'];

      setState(() {
        loading = false;

        if(responseBody != null){
          debtors = responseBody;

        }



      });
    }
    if(sortType == 'product'){
      var res = await http.get('${endpointApi}/processed_products/remainders?sort_type=sum&sort_value=desc',headers: {
        "Accept" : "Application/json",
        "Authorization" : "Bearer $token"
      },);
      if(date != null){
        var res = await http.get(
          '$endpointApi/processed_products/remainders?sort_type=sum&sort_value=desc&date[]=${date[0][0]}&date=${date[0][1]}',
          headers: {
            "Accept" : "Application/json",
            "Authorization" : "Bearer $token"
          },
        );
      }
      var w = MediaQuery.of(context).size.width;
      var responseBody = jsonDecode(res.body)['products'];
      print(responseBody);
      setState(() {
        loading = false;

        if(responseBody != null){
          debtors = responseBody;


        }



      });
    }

    return 'success';
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    this.getDebtors();
  }
  @override

  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Топ продаж'),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: RefreshIndicator(
          onRefresh: ()async{
            this.getDebtors();

          },
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),

            child: Container(
              width: w,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical:30.0,horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Center(
                      child: Text(
                        'Топ продаж',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w500,

                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: ()async{
                        audioPlay();

                        final List<DateTime> picked = await DateRagePicker.showDatePicker(
                            context: context,
                            initialFirstDate: dateInitial,
                            initialLastDate: lastDate,
                            firstDate: new DateTime(2015),
                            lastDate: new DateTime(2050));

                        if(picked != null && picked.length == 2){
                          setState(() {
                            dateInitial = picked[0];
                            lastDate = picked[1];
                          });
                          this.getDebtors([picked]);
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Выберите период:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text('${dateInitial.day} ${months[dateInitial.month-1]} - ${lastDate.day} ${months[lastDate.month-1]}',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),

                              SizedBox(width: 16,),
                              Icon(Icons.calendar_today,color: Theme.of(context).accentColor,),
                            ],
                          )

                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonSells(
                          text: 'по клиентам',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              this.sortType = 'name';
                              this.getDebtors();
                            });
                          },
                          width: w/3-20,
                          color:  sortType == 'name' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor: sortType == 'name' ? Colors.white : Colors.black,
                          fontSize: 14.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur: sortType  == 'name' ? true :false,


                        ),
//                      ButtonSells(
//                        text: 'по товарам',
//                        onTap:(){},
//                        width: w/3-20,
//                        color: Colors.white,
//                        height: 50.0,
//                        textColor: Colors.black,
//                        fontSize: 14.0,
//                        spreadRadius: 0.0,
//                        borderColor: Theme.of(context).primaryColor,
//                        enableBlur: false,
//
//
//                      ),
                        ButtonSells(
                          text: 'по товарам',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              this.sortType = 'product';
                              this.getDebtors();
                            });
                          },
                          width: w/3-20,
                          color:  sortType == 'product' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor: sortType == 'product' ? Colors.white : Colors.black,
                          fontSize: 14.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur: sortType  == 'product' ? true :false,


                        ),
                        ButtonSells(
                          text: 'по сумме',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              this.sortType = 'total_sum';
                              this.getDebtors();
                            });
                          },
                          width: w/3-20,
                          color:  sortType == 'total_sum' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor: sortType == 'total_sum' ? Colors.white : Colors.black,
                          fontSize: 14.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur: sortType  == 'total_sum' ? true :false,


                        ),
                      ],
                    ),
                    SizedBox(height: 30,),
                    Container(
                      width: w,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset: Offset(0,1)
                            )
                          ]
                      ),
                      child: Container(
                        child:debtors != null ? debtors.length !=0 && loading == false  ?
                        Column(
                          children: <Widget>[

                            ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: debtors.length,
                              itemBuilder: (context,index){
                                return( GestureDetector(
                                  onTap: ()=>{


                                  },
                                  child: Container(

                                    height: 80,
                                    width: w,
                                    decoration: BoxDecoration(
                                        color: index == 0 ? Color(0xffF2FBFF) : Colors.white
                                    ),
                                    padding: EdgeInsets.symmetric(horizontal: 16,vertical: 10),
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('${index+1}',style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,

                                            ),),
                                            SizedBox(width: 16,),
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text('${sortType =='name' ? debtors[index]['name'] : sortType == 'product' ?"${debtors[index]['title']} (${debtors[index]['amount'] } шт)" :debtors[index]['partner_id']}',style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w500
                                                ),),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Row(
                                                  children: [
                                                    PriceDebtors( sortType =='name' ?
                                                    debtors[index]['debt'].toString(): sortType == 'product' ? debtors[index]['price'] != null ?
                                                    (debtors[index]['amount'].abs()*debtors[index]['price']).toString() : '' : debtors[index]['total_sum'].toString()),
                                                    SizedBox(width: 16,),

                                                  ],
                                                )
                                              ],
                                            )
                                          ],

                                        ),


                                      ],
                                    ),
                                  ),
                                ));
                              },

                            ),
                          ],
                        ) :
                        Center(
                          child: Container(
                            padding: EdgeInsets.all(50),
                              child: CircularProgressIndicator(
                                backgroundColor: Theme.of(context).primaryColor,
                              ),
                          ),
                        ):
                        Padding(
                          padding: const EdgeInsets.all(40.0),
                          child: Center(
                            child:Text('Пока что нет накладных')
                          ),
                        )
                      ),



                    ),


                  ],

                ),
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: w,
        height: 125,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical:25.0),
          child: Column(
            children: [
              Center(
                child:ButtonSells(
                  text: 'Экспортировать в .pdf',
                  onTap:() async{
                    audioPlay();

                  List<pw.TableRow> pdfWidgets = [];
                  final doc = pw.Document();
                  final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
                  final ttf = pw.Font.ttf(font);
                  if(sortType == 'name' || sortType=='total_sum'){
                    pdfWidgets.add(pw.TableRow(
                        children: [
                          pw.Text('Имя партнера',style: pw.TextStyle(font: ttf,fontSize: 18)),
                          pw.Text('Сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),


                        ]
                    ));
                  }else{
                    pdfWidgets.add(pw.TableRow(
                        children: [
                          pw.Text('Название товара',style: pw.TextStyle(font: ttf,fontSize: 18)),
                          pw.Text('Сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),


                        ]
                    ));
                  }
                  debtors.forEach((element) {
                    if(sortType == 'name'){
                      pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('${element['name']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                pw.Text('${element['debt']} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),



                              ]
                          )
                      );
                    }

                    if(sortType == 'total_sum'){
                      pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('${element['partner_id']}',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                pw.Text('${element['total_sum']} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                              ]
                          )
                      );
                    }
                    if(sortType == 'product'){
                      pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('${element['title']}',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                pw.Text('${element['amount']*element['price']} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                              ]
                          )
                      );
                    }
                  });
                  doc.addPage(
                    pw.Page(
                        build: (pw.Context context) =>pw.Column(
                            children: [
                              pw.Table(
                                  border: pw.TableBorder(),
                                  children: pdfWidgets

                              ),

                            ]
                        )
                    ),

                    );
//                    await Printing.layoutPdf  (
//                      onLayout: ( format) async => doc.save());
                    await Printing.sharePdf(bytes: doc.save(), filename: 'my-document.pdf');
                  },
                  width: w-32,
                  color: Theme.of(context).primaryColor,
                  height: 70.0,
                  textColor: Colors.white,
                  fontSize: 18.0,
                  spreadRadius: 0.0,
                  borderColor: Theme.of(context).primaryColor,
                  enableBlur: false,


                ),
              ),

            ],
          ),
        ),

      ),
    );

  }
}
class PriceDebtors extends StatelessWidget {
  String sum;

  PriceDebtors(this.sum);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Color(0xffEBF9FF),
      ),
      child: Center(child:
      Text(
        '${this.sum} ₸',style: TextStyle(
          color: Theme.of(context).primaryColor,

          fontSize: 14
      ),
      ),),
    );
  }
}
