import 'dart:convert';

import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

import '../../Setting.dart';

class LendersPage extends StatefulWidget {
  static var id = 'lenders';

  @override
  _LendersPageState createState() => _LendersPageState();
}

class _LendersPageState extends State<LendersPage> {
  List<dynamic> lendors;
  bool paid = false;
  bool notPaid = false;
  String sortValue = 'desc';
  String sortType = 'debt';

  DateTime dateInitial = DateTime.now().subtract(new Duration(days:30));
  DateTime lastDate = DateTime.now();

  List<String> months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];
  bool loading = false;
  Future<dynamic> getDebtors([List<dynamic> date])async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      loading = true;

    });

    final token = prefs.getString('token');
    if(sortType != 'product'){
      var res = await http.get('${endpointApi}/partners/creditors?sort_type=${sortType == 'title' ? 'name' : sortType}&sort_value=${this.sortValue}',headers: {
        "Accept" : "Application/json",
        "Authorization" : "Bearer $token"

      },);
      if(date != null){
        var res = await http.get(
          '$endpointApi/partners/creditors?sort_type=${sortType == 'title' ? 'name' : sortType }&sort_value=${this.sortValue}&date[]=${date[0][0]}&date=${date[0][1]}',
          headers: {
            "Accept" : "Application/json",
            "Authorization" : "Bearer $token"
          },
        );
      }

      var w = MediaQuery.of(context).size.width;
      print(res.body);
      var responseBody = jsonDecode(res.body);

      setState(() {
        print(responseBody);
        lendors = responseBody;


      });
    }else{
      var res = await http.get('${endpointApi}/processed_products/remainders?sort_type=sum&sort_value=desc',headers: {
        "Accept" : "Application/json",
        "Authorization" : "Bearer $token"
      },);

      if(date != null){
        var res = await http.get(
          '$endpointApi/processed_products/remainders?sort_type=sum&sort_value=desc&date[]=${date[0][0]}&date=${date[0][1]}',
          headers: {
            "Accept" : "Application/json",
            "Authorization" : "Bearer $token"
          },
        );
      }
      var responseBody = jsonDecode(res.body)['products'];
      print(responseBody[1]);
      setState(() {
        lendors = responseBody;


      });
    }
    setState(() {
      loading = false;

    });

    return 'success';
  }

  @override
  void initState() {
    // TODO: implement initState  
    super.initState();
    this.getDebtors();
    this.sortValue = 'desc';

  }

  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Топ кредиторов'),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: RefreshIndicator(
        onRefresh: ()async{
          this.getDebtors();
        },
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),

            child: Container(
              color: Colors.white,
              width: w,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical:30.0,horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [


                    Center(
                      child: Text(
                        'Топ кредиторов',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w500,

                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    GestureDetector(

                      onTap:()async{
                        audioPlay();

                        final List<DateTime> picked = await DateRagePicker.showDatePicker(
                            context: context,
                            initialFirstDate: dateInitial,
                            initialLastDate: lastDate,
                            firstDate: new DateTime(2015),
                            lastDate: new DateTime(2050));

                        if(picked != null && picked.length == 2){
                          dateInitial = picked[0];
                          lastDate = picked[1];
                          this.getDebtors([picked]);
                        }

                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Выберите период:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text('${dateInitial.day} ${months[dateInitial.month-1]} - ${lastDate.day} ${months[lastDate.month-1]}',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),

                              SizedBox(width: 16,),
                              Icon(Icons.calendar_today,color: Theme.of(context).accentColor,),
                            ],
                          )

                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonSells(
                          text: 'по сумме',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              if(this.sortType == 'debt' && this.sortValue == 'asc'){
                                this.sortValue = 'desc';
                              }else{
                                this.sortValue = 'asc';

                              }
                              this.sortType = 'debt';
                              this.getDebtors();
                            });
                          },
                          width: w/3-25,
                          color: this.sortType == 'debt' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor:this.sortType == 'debt'? Colors.white: Colors.black,
                          fontSize: 16.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur:this.sortType == 'debt' ?  true : false,
//                        icon: this.sortValue=='asc' ? Icons.arrow_drop_down :  Icons.arrow_drop_up,


                        ),
                        ButtonSells(
                          text: 'по товарам',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              if(this.sortType == 'title' && this.sortValue == 'asc'){
                                this.sortValue = 'desc';
                              }else{
                                this.sortValue = 'asc';

                              }
                              this.sortType = 'product';


                              this.getDebtors();
                            });
                          },
                          width: w/3-25,
                          color: this.sortType == 'product' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor:this.sortType == 'product'?Colors.white: Colors.black,
                          fontSize: 16.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur: this.sortType == 'product'? true : false,
//                        icon: this.sortValue=='asc' ? Icons.arrow_drop_up :  Icons.arrow_drop_down,



                        ),
                        ButtonSells(
                          text: 'по клиентам',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              if(this.sortType == 'name' && this.sortValue == 'asc'){
                                this.sortValue = 'desc';
                              }else{
                                this.sortValue = 'asc';

                              }
                              this.sortType = 'name';


                              this.getDebtors();
                            });
                          },
                          width: w/3-25,
                          color: this.sortType == 'name' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor:this.sortType == 'name'?Colors.white: Colors.black,
                          fontSize: 16.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur: this.sortType == 'name'? true : false,
//                        icon: this.sortValue=='asc' ? Icons.arrow_drop_up :  Icons.arrow_drop_down,



                        ),
                      ],
                    ),
                    SizedBox(height: 30,),
                    Container(
                      width: w,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset: Offset(0,1)
                            )
                          ]
                      ),
                      child:loading != true ?
                      loading == null  && lendors.length==0 ?
                      Padding(
                          padding: const EdgeInsets.all(25) ,
                          child: Center(
                              child: Text('Накладных нет')
                          )
                      ):
                      Container(

                        child: Column(
                          children: <Widget>[

                            ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount:lendors != null ? lendors.length : 0,
                              itemBuilder: (context,index) {
                                return (
                                 GestureDetector(
                                  onTap: () =>
                                  {

                                  },
                                  child: Container(

                                    height: 80,
                                    width: w,
                                    decoration: BoxDecoration(
                                        color: index != 0 ? Colors.white : Color(0xffF2FBFF)
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 10),
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('${index+1}', style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,

                                            ),),
                                            SizedBox(width: 16,),
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .start,
                                              children: [
                                                Text('${sortType == 'product' ? lendors[index]['title'] :lendors[index]['name']}',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.w500
                                                  ),),
                                                SizedBox(
                                                  height: 8,
                                                ),
                                                Row(
                                                  children: [
                                                    PriceDebtors(price: sortType == 'product' ? (lendors[index]['price']*lendors[index]['amount']).toString(): lendors[index]['debt'],),
                                                    SizedBox(width: 16,),

                                                  ],
                                                )
                                              ],
                                            )
                                          ],

                                        ),


                                      ],
                                    ),
                                  ),
                                )
                                );
                              }
                              ),
                          ],
                        ),
                      ) :
                        Container(
                          padding: EdgeInsets.all(50),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )


                      ),





                  ],

                ),
              ),
            ),
          ),
        ),

      bottomNavigationBar: Container(
        width: w,
        height: 125,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical:25.0),
          child: Column(
            children: [
              Center(
                child:ButtonSells(
                  text: 'Экспортировать в .pdf',
                  onTap:() async{
                    audioPlay();

                    List<pw.TableRow> pdfWidgets = [];
                    final doc = pw.Document();
                    final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
                    final ttf = pw.Font.ttf(font);
                    if(sortType != 'product'){
                      pdfWidgets.add(pw.TableRow(
                          children: [
                            pw.Text('Имя партнера',style: pw.TextStyle(font: ttf,fontSize: 18)),
                            pw.Text('Сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),


                          ]
                      ));
                      lendors.forEach((element) {
                        pdfWidgets.add(
                            pw.TableRow(
                                children: [
                                  pw.Text('${element['name']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                  pw.Text('${int.parse(element['debt']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),



                                ]
                            )
                        );


                      });
                    }else{
                      pdfWidgets.add(pw.TableRow(
                          children: [
                            pw.Text('Имя продукта',style: pw.TextStyle(font: ttf,fontSize: 18)),
                            pw.Text('Сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),


                          ]
                      ));
                      lendors.forEach((element) {
                        pdfWidgets.add(
                            pw.TableRow(
                                children: [
                                  pw.Text('${element['title']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                  pw.Text('${(element['amount'].abs()*element['price'].abs())} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),



                                ]
                            )
                        );


                      });
                    }
                    doc.addPage(
                      pw.Page(
                          build: (pw.Context context) =>pw.Column(
                              children: [
                                pw.Table(
                                    border: pw.TableBorder(),
                                    children: pdfWidgets

                                ),

                              ]
                          )
                      ),

                    );
//                    await Printing.layoutPdf  (
//                        onLayout: ( format) async => doc.save());

                    await Printing.sharePdf(bytes: doc.save(), filename: 'my-document.pdf');
                  },
                  width: w-32,
                  color: Theme.of(context).primaryColor,
                  height: 70.0,
                  textColor: Colors.white,
                  fontSize: 18.0,
                  spreadRadius: 0.0,
                  borderColor: Theme.of(context).primaryColor,
                  enableBlur: false,


                ),
              ),

            ],
          ),
        ),

      )
    );

  }
}
class PriceDebtors extends StatelessWidget {
  var price;


  PriceDebtors({Key key ,this.price}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Color(0xffEBF9FF),
      ),
      child: Center(child:
      Text(
        '${int.parse(this.price).abs().toString()} ₸',style: TextStyle(
          color: Theme.of(context).primaryColor,

          fontSize: 14
      ),
      ),),
    );
  }
}
