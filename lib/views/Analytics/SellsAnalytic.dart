import 'dart:convert';

import 'package:farcount/provider/AuthProvider.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Setting.dart';
import '../OweList.dart';
import '../OwePersonView.dart';
import 'package:http/http.dart' as http;
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class SellsAnalyticPage extends StatefulWidget {
  static var id = 'sells_analytic';

  @override
  _SellsAnalyticPageState createState() => _SellsAnalyticPageState();
}

class _SellsAnalyticPageState extends State<SellsAnalyticPage> {
  String sortType = 'name';
  List<dynamic> debtors = [];
  String profit = '';
  String profitSum = '';
  bool loading = false;
  bool profitLoader = false;
  List<String> months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];
  DateTime lastDate = DateTime.now();
  DateTime initialDate = DateTime.now().subtract(new Duration(days: 30));
  DateTime lastDateProfit = DateTime.now();
  DateTime initialDateProfit = DateTime.now().subtract(new Duration(days: 30));

  DateTime lastDateProfitSum = DateTime.now();
  DateTime initialDateProfitSum = DateTime.now().subtract(new Duration(days: 30));
  Future<dynamic> getDebtors([List<dynamic> date])async{
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();


    final token = prefs.getString('token');
    var res = await http.get('${endpointApi}/partners/debtors?sort_type=${sortType}&sort_value=desc${term.text != '' ? '&term=${term.text}':''}',
      headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"

    },);
    var w = MediaQuery.of(context).size.width;
    if(date != null){
      print(date[0][1]);
      res = await http.get(
        '$endpointApi/partners/debtors?sort_type=$sortType&sort_value=desc&date[]=${date[0][0]}&date[]=${date[0][1]}${term.text != '' ? '&term=${term.text}':''}',
        headers: {
          "Accept" : "Application/json",
          "Authorization" : "Bearer $token"
        },
      );
    }

    var responseBody = jsonDecode(res.body);
    print(responseBody);
    setState(() {
      if(responseBody.length>0){
        debtors = responseBody;
      }else{
        debtors = null;
      }
      var i = 1;
      loading = false;

    });

    return 'success';
  }
  bool profitSumLoader = false;


  TextEditingController term = TextEditingController();

  Future<dynamic> getProfit(date) async{
    setState(() {
      profitLoader = true;
    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    var res = await http.get('${endpointApi}/waybills/profit?date[]=$initialDateProfit&date[]=$lastDateProfit',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"

    },);
    var responseBody = jsonDecode(res.body);
    this.setState(() {
      profit = responseBody['profit'].round().toString();
      profitLoader = false;
    });

  }

  Future<dynamic> getProfitSum(date)async{
    setState(() {
      profitSumLoader = true;

    });
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    var res = await http.get('${endpointApi}/waybills/sum-profit?date[]=$initialDateProfitSum&date[]=$lastDateProfitSum',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"

    },);
    var responseBody = jsonDecode(res.body);
    print(responseBody);
    this.setState(() {
      profitSum = responseBody['profit'].round().toString();
      print(profitSum);

      profitSumLoader = false;
    });


  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getDebtors();
    this.getProfit([initialDate,lastDate]);
    this.getProfitSum([initialDateProfitSum,lastDateProfitSum]);
  }
  @override
  Widget build(BuildContext context) {

    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Анализ продаж'),
        centerTitle: true,
      ),

      body: RefreshIndicator(
        onRefresh: ()async{
          this.getDebtors();
          this.getProfit([initialDate,lastDate]);
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top:20),
                  width: w -32,
                  height: 70,
                  child: TextField(
                    controller: term,
                    cursorColor: Colors.black,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                        borderRadius: new BorderRadius.circular(5.0),
                      ),
                      hintText: 'Поиск по партнерам',

                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                      ),
                      suffixIcon: Icon(Icons.search,color: Color(0xffDFE0E3)),
                    ),
                    keyboardType: TextInputType.text,
                    style: new TextStyle(
                      color: Colors.black,
                    ),
                    onChanged: (val){
                      setState(() {
                        this.getDebtors();
                      });
                    },
                  ),
                ),
                SizedBox(height: 20,),
                GestureDetector(
                  onTap: ()async{
                    audioPlay();

                    final List<DateTime> picked = await DateRagePicker.showDatePicker(
                        context: context,
                        initialFirstDate: initialDate,
                        initialLastDate: lastDate,
                        firstDate: new DateTime(2015),
                        lastDate: new DateTime(2050));

                    if(picked != null && picked.length == 2){
                      setState(() {
                        initialDate = picked[0];
                        lastDate = picked[1];
                      });
                      print(picked);
                      this.getDebtors([picked]);
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Выберите период:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            '${initialDate.day} ${months[initialDate.month-1]}- ${lastDate.day} ${months[lastDate.month-1]}',
                            style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor
                            ),
                          ),                        SizedBox(width: 16,),
                          Icon(Icons.calendar_today,color: Theme.of(context).accentColor,),
                        ],
                      )

                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ButtonSells(
                      text: 'По клиентам',
                      onTap:(){
                        audioPlay();

                        setState(() {
                          this.sortType = 'name';
                          this.getDebtors();
                        });
                      },
                      width: w/2-20,
                      color:  sortType == 'name' ? Theme.of(context).primaryColor : Colors.white,
                      height: 50.0,
                      textColor: sortType == 'name' ? Colors.white : Colors.black,
                      fontSize: 14.0,
                      spreadRadius: 0.0,
                      borderColor: Theme.of(context).primaryColor,
                      enableBlur: sortType  == 'name' ? true :false,


                    ),
//                  ButtonSells(
//                    text: 'По товарам',
//                    onTap:(){
//                      setState(() {
//                        this.sortType = 'products';
//                      });
//                    },
//                    width: w/3-20,
//                    color:  sortType == 'products' ? Theme.of(context).primaryColor : Colors.white,
//                    height: 50.0,
//                    textColor: sortType == 'products' ? Colors.white : Colors.black,
//                    fontSize: 14.0,
//                    spreadRadius: 0.0,
//                    borderColor: Theme.of(context).primaryColor,
//                    enableBlur: sortType  == 'products' ? true :false,
//
//
//                  ),
                    ButtonSells(
                      text: 'По сумме',
                      onTap:(){
                        audioPlay();

                        setState(() {
                          sortType = 'debt';
                          this.getDebtors();
                        });
                      },
                      width: w/2-20,
                      color:  sortType == 'debt' ? Theme.of(context).primaryColor : Colors.white,
                      height: 50.0,
                      textColor: sortType == 'debt' ? Colors.white : Colors.black,
                      fontSize: 14.0,
                      spreadRadius: 0.0,
                      borderColor: Theme.of(context).primaryColor,
                      enableBlur: sortType  == 'debt' ? true :false,



                    ),
                  ],
                ),
                SizedBox(height: 20,),

                Container(

                      width: w,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 1,
                            blurRadius: 10,
                            offset: Offset(0,1)
                          )
                        ]
                      ),
                      child: Container(
                        child: debtors != null ? loading != true  ?
                        Column(
                          children: <Widget>[
                            ListView.builder(

                              itemCount: debtors.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context,index){
                                return (
                                    GestureDetector(
                                      onTap: ()=>{

//                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>OwePersonView(waybill: debtors[index]['id'],)))
                                      },
                                      child: Container(

                                        height: 80,
                                        width: w,
                                        decoration: BoxDecoration(
                                            color:index == 0 ? Color(0xffF2FBFF) : Colors.white
                                        ),
                                        padding: EdgeInsets.symmetric(horizontal: 16,vertical: 10),
                                        child: Stack(

                                          alignment: Alignment.center,
                                          children: [
                                            Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text('${index+1}',style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w500,

                                                ),),
                                                SizedBox(width: 16,),
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text('${debtors[index]['name']}',style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 18,
                                                        fontWeight: FontWeight.w500
                                                    ),),
                                                    SizedBox(
                                                      height: 8,
                                                    ),
                                                    Row(
                                                      children: [
                                                        PriceContainer(debtors[index]['debt'],0),
                                                        SizedBox(width: 16,),

                                                      ],
                                                    )
                                                  ],
                                                )
                                              ],

                                            ),


                                          ],
                                        ),
                                      ),
                                    )

                                );
                              },
                            ),
                          ],
                        ) :
                        Center(
                            child: Container(
                              padding: EdgeInsets.all(100),
                              child: CircularProgressIndicator(
                                backgroundColor: Theme.of(context).primaryColor,
                              ),
                            ),
                        )
                            :
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Center(
                            child: Text('Пока что нет данных'),
                          ),
                        )
                      ),


                ),
                SizedBox(height: 20,),
                Center(
                  child: Column(
                    children: [
                      Text('Продажи за период',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w500,color: Colors.black),),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Выберите период:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                          GestureDetector(
                            onTap: ()async{
                              audioPlay();

                              final List<DateTime> picked = await DateRagePicker.showDatePicker(
                              context: context,
                              initialFirstDate: initialDateProfit,
                              initialLastDate: lastDateProfit,
                              firstDate: new DateTime(2015),
                              lastDate: new DateTime(2050));

                              if(picked != null && picked.length == 2) {
                                setState(() {
                                  initialDateProfit = picked[0];
                                  lastDateProfit = picked[1];
                                  this.getProfit([initialDateProfit,lastDateProfit]);

                                });

                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  '${initialDateProfit.day} ${months[initialDateProfit.month-1]}- ${lastDateProfit.day} ${months[lastDateProfit.month-1]}',
                                  style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor
                                  ),
                                ),
                                SizedBox(width: 16,),
                                Icon(Icons.calendar_today,color: Theme.of(context).accentColor,),
                              ],
                            ),
                          )

                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  width: w,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black12,
                        spreadRadius: 1,
                        blurRadius: 10,
                        offset: Offset(0,1)
                      )
                    ]
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical:15.0,),
                    child: Center(
                      child: profitLoader == true ?
                      CircularProgressIndicator() :
                      Text('$profit ₸',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),)
                    ),
                  ),

                ),
                SizedBox(height: 20,),

                Center(
                  child: Column(
                    children: [
                      Text('Маржа за период',style: TextStyle(fontSize: 24,fontWeight: FontWeight.w500,color: Colors.black),),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Выберите период:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                          GestureDetector(
                            onTap: ()async{
                              audioPlay();

                              final List<DateTime> picked = await DateRagePicker.showDatePicker(
                                  context: context,
                                  initialFirstDate: initialDateProfitSum,
                                  initialLastDate: lastDateProfitSum,
                                  firstDate: new DateTime(2015),
                                  lastDate: new DateTime(2050));

                              if(picked != null && picked.length == 2) {
                                setState(() {
                                  initialDateProfitSum = picked[0];
                                  lastDateProfitSum = picked[1];
                                  this.getProfitSum([initialDateProfitSum,lastDateProfitSum]);

                                });

                              }
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  '${initialDateProfitSum.day} ${months[initialDateProfitSum.month-1]}- ${lastDateProfitSum.day} ${months[lastDateProfitSum.month-1]}',
                                  style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor
                                  ),
                                ),
                                SizedBox(width: 16,),
                                Icon(Icons.calendar_today,color: Theme.of(context).accentColor,),
                              ],
                            ),
                          )

                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  width: w,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 1,
                            blurRadius: 10,
                            offset: Offset(0,1)
                        )
                      ]
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical:15.0,),
                    child: Center(
                        child: profitSumLoader == true ?
                        CircularProgressIndicator() :
                        Text('$profitSum ₸',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),)
                    ),
                  ),

                ),
                SizedBox(
                  height: 80,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ButtonSells extends StatelessWidget {
  final text;
  final onTap;
  final width;
  final color;
  final height;
  final textColor;
  final fontSize;
  final spreadRadius;
  final borderColor;
  final enableBlur;
  final icon;
  ButtonSells({
    this.text,
    this.onTap,
    this.width,
    this.color,
    this.height,
    this.textColor,
    this.fontSize,
    this.spreadRadius,
    this.borderColor,
    this.enableBlur,
    this.icon});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: this.onTap,
      child: Container(

        width: this.width,
        height: this.height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: this.color,
            border: Border.all(
              color: this.enableBlur ?  Colors.transparent : this.borderColor,
              width: 1,
            ),
            boxShadow: [
              BoxShadow(
                color:  this.borderColor.withOpacity(0.5),
                spreadRadius: this.spreadRadius,
                blurRadius: this.enableBlur ? 8 : 0,
                offset: Offset(0, 1),
                // changes position of shadow
              ),
            ],

        ),
        child: Row(
          mainAxisAlignment: this.icon != null ? MainAxisAlignment.spaceEvenly:MainAxisAlignment.center,

          children: [
            text != '' ? Text(text,style: TextStyle(
                fontSize: this.fontSize,
                color: this.textColor
            ),) : Container(
                child: CircularProgressIndicator(backgroundColor: Colors.white,)),
            this.icon != null ? Icon(this.icon,color: this.textColor,): SizedBox.shrink()

          ],
        ),
      ),
    );
  }


}