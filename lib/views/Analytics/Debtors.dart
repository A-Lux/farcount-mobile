import 'dart:convert';

import 'package:farcount/Widgets/Headings.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/Page.dart';
import 'package:farcount/views/Analytics/SellsAnalytic.dart';
import 'package:farcount/views/OweList.dart';
import 'package:farcount/views/OwePersonView.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

import '../../Setting.dart';

class DebtorsPage extends StatefulWidget {
  static var id = 'debtors';

  @override
  _DebtorsPageState createState() => _DebtorsPageState();
}

class _DebtorsPageState extends State<DebtorsPage> {
  DateTime dateInitial = DateTime.now().subtract(new Duration(days:30));
  DateTime lastDate = DateTime.now();

  List<String> months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];
  List<dynamic> debtors;
  List<Widget> debtorsWidget = [];
  String sortValue = 'desc';
  bool loading = false;
  String sortType = 'debt';
  Future<dynamic> getDebtors([List<dynamic> date])async{
    setState(() {
      loading = true;
    });
    final prefs = await SharedPreferences.getInstance();


    final token = prefs.getString('token');
    var res = await http.get('$endpointApi/partners/debtors?sort_type=$sortType&sort_value=${this.sortValue}',headers: {
      "Accept" : "Application/json",
      "Authorization" : "Bearer $token"
    },);
    if(date != null){
      var res = await http.get(
        '$endpointApi/partners/debtors?sort_type=$sortType&sort_value=${this.sortValue}&date[]=${date[0][0]}&date=${date[0][1]}',
        headers: {
        "Accept" : "Application/json",
        "Authorization" : "Bearer $token"
        },
      );
    }
    var w = MediaQuery.of(context).size.width;
    print(res.body.toString());
    var responseBody = jsonDecode(res.body);
    setState(() {

      debtors = responseBody;
      loading = false;
      var i = 1;


    });


    return 'success';
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getDebtors();
    this.sortValue = 'desc';

  }
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  _selectDate(BuildContext context) async{
    final DateTime picked =await showDatePicker(
        context: context,
        helpText: 'Выберите день',
        confirmText: 'Выбрать',
        locale: const Locale('ru',"RU"),

        initialDate: dateInitial,
        firstDate: DateTime(2000),
        cancelText: 'Отменить',
        lastDate: DateTime(2025));

  }

  @override

  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Топ должников'),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: RefreshIndicator(
          key: refreshKey,
          onRefresh: ()async{

            this.getDebtors();

          },
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),

            child: Container(
              width: w,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical:30.0,horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [



                    Center(
                      child: Text(
                        'Топ должников',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w500,

                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap:()async{
                        audioPlay();

                        final List<DateTime> picked = await DateRagePicker.showDatePicker(
                            context: context,
                            initialFirstDate: dateInitial,
                            initialLastDate: lastDate,
                            firstDate: new DateTime(2015),
                            lastDate: new DateTime(2050));

                        if(picked != null && picked.length == 2){
                          setState(() {
                            dateInitial = picked[0];
                            lastDate = picked[1];
                          });
                          this.getDebtors([picked]);
                        }

                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Выберите период:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text('${dateInitial.day} ${months[dateInitial.month-1]} - ${lastDate.day} ${months[lastDate.month-1]}',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Theme.of(context).accentColor),),
                              SizedBox(width: 16,),
                              Icon(Icons.calendar_today,color: Theme.of(context).accentColor,),
                            ],
                          )

                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonSells(
                          text: 'по сумме долга',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              if(this.sortType == 'debt' && this.sortValue == 'asc'){
                                this.sortValue = 'desc';
                              }else{
                                this.sortValue = 'asc';

                              }
                              this.sortType = 'debt';
                              this.getDebtors();
                            });
                          },
                          width: w/2-20,
                          color: this.sortType == 'debt' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor:this.sortType == 'debt'? Colors.white: Colors.black,
                          fontSize: 16.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur:this.sortType == 'debt' ?  true : false,
                          icon: this.sortValue=='asc' ? Icons.arrow_drop_down :  Icons.arrow_drop_up,


                        ),
                        ButtonSells(
                          text: 'по имени',
                          onTap:(){
                            audioPlay();

                            setState(() {
                              if(this.sortType == 'name' && this.sortValue == 'asc'){
                                this.sortValue = 'desc';
                              }else{
                                this.sortValue = 'asc';

                              }
                              this.sortType = 'name';


                              this.getDebtors();
                            });
                          },
                          width: w/2-20,
                          color: this.sortType == 'name' ? Theme.of(context).primaryColor : Colors.white,
                          height: 50.0,
                          textColor:this.sortType == 'name'?Colors.white: Colors.black,
                          fontSize: 16.0,
                          spreadRadius: 0.0,
                          borderColor: Theme.of(context).primaryColor,
                          enableBlur: this.sortType == 'name'? true : false,
                          icon: this.sortValue=='asc' ? Icons.arrow_drop_up :  Icons.arrow_drop_down,



                        ),
                      ],
                    ),
                    SizedBox(height: 30,),
                    Container(
                      width: w,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset: Offset(0,1)
                            )
                          ]
                      ),
                      child:loading != true ? debtors != null ?
                          Container(
                            child: Column(
                              children: <Widget>[
                                ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: debtors.length,
                                    itemBuilder: (context,index){
                                    return (

                                      GestureDetector(
                                        onTap: ()=>{
                                        },
                                        child: Container(
                                          height: 80,
                                          width: w,
                                          decoration: BoxDecoration(
                                            border:  Border(bottom: BorderSide(color: Color(0xffDFE0E3))) ,
                                            color: index == 0 ? Color(0xffFFF2F5) : Colors.white
                                          ),
                                          padding: EdgeInsets.symmetric(horizontal: 16,vertical: 10),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text('${index+1}',style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w500,

                                                  ),),
                                                  SizedBox(width: 16,),
                                                  Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text('${debtors[index]['name']}',style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 18,
                                                      ),),
                                                      SizedBox(
                                                        height: 8,
                                                      ),
                                                      Row(
                                                        children: [
                                                          PriceDebtors(debtors[index]['debt']),
                                                          SizedBox(width: 16,),

                                                        ],
                                                      )
                                                    ],
                                                  )
                                                ],

                                              ),


                                            ],
                                          ),
                                        ),
                                      )

                                  );
                                }),
                              ],
                            ),
                          ) :
                          Center(child: Text('Пока что нет продуктов')
                      ):Container(
                        padding: EdgeInsets.all(100),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),


                  ],

                ),
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: w,
        height: 125,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical:25.0),
          child: Column(
            children: [
              Center(
                child:ButtonSells(
                  text: 'Экспортировать в .pdf',
                  onTap:() async{
                    audioPlay();

                    List<pw.TableRow> pdfWidgets = [];
                    final doc = pw.Document();
                    final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
                    final ttf = pw.Font.ttf(font);
                    pdfWidgets.add(pw.TableRow(
                        children: [
                          pw.Text('Имя партнера',style: pw.TextStyle(font: ttf,fontSize: 18)),
                          pw.Text('Сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),


                        ]
                    ));
                    debtors.forEach((element) {
                      pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('${element['name']}',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                pw.Text('${element['debt']} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),



                              ]
                          )
                      );


                    });
                    doc.addPage(
                      pw.Page(
                          build: (pw.Context context) =>pw.Column(
                              children: [
                                pw.Table(
                                    border: pw.TableBorder(),
                                    children: pdfWidgets

                                ),

                              ]
                          )
                      ),

                    );
//                    await Printing.layoutPdf  (
//                        onLayout: ( format) async => doc.save());

                    await Printing.sharePdf(bytes: doc.save(), filename: 'my-document.pdf');
                  },
                  width: w-32,
                  color: Theme.of(context).primaryColor,
                  height: 70.0,
                  textColor: Colors.white,
                  fontSize: 18.0,
                  spreadRadius: 0.0,
                  borderColor: Theme.of(context).primaryColor,
                  enableBlur: false,


                ),
              ),

            ],
          ),
        ),

      ),
    );

  }
}


class PriceDebtors extends StatelessWidget {
  int sum;


  PriceDebtors(this.sum);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 28,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Color(0xffffffff),
      ),
      child: Center(child:
      Text(
        '${sum.toString()} ₸',style: TextStyle(
          color: Color(0xffFF0C46),
          fontSize: 14
      ),
      ),),
    );
  }
}
