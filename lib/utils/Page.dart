import 'package:farcount/LOGINPAGES/WelcomeScreen.dart';
import 'package:farcount/views/HelpWebView.dart';
import 'package:flutter/material.dart';
import 'package:farcount/AppBarMain.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../Setting.dart';

class AppPage extends StatefulWidget {
  final Widget content;
  AppPage({ Key key, this.content, }) : super(key: key);

  @override
  _AppPageState createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  bool sound=false;
  getSoundValue()async{
    final prefs = await SharedPreferences.getInstance();
    sound = prefs.getBool('audio');
  }
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getSoundValue();
  }
  void setAudio(audio)async{
    setState(() {
      sound = !sound;

    });
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('audio',sound);
    audioPlay();
  }

  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AluxAppBar(),
        body: widget.content,
      drawer: Drawer(
        child: SafeArea(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
               height: h/2-90,
                color: Colors.white,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      top: 16,
                      left: 16,
                      child:
                      GestureDetector(
                          onTap: (){
                            audioPlay();

                            Navigator.pop(context);
                          },
                          child: Icon(Icons.close,color: Theme.of(context).primaryColor,size: 35,)
                      ),
                    ),
                    Image(image: AssetImage('assets/logo.png',),width: w/2,)
                  ],
                ),
              ),
              Container(
                height: 60,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: Color(0xffDFE0E3)))
                ),
                child: ListTile(
                  title: Text('О приложении'),
                  onTap: () {
                    audioPlay();

                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>HelpPage(title:'О приложении',url: '/about',)));
                  },
                  leading: Icon(Icons.get_app,color: Color(0xfffD9E5EA),),
                ),
              ),
              Container(
                height: 60,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: Color(0xffDFE0E3)))
                ),
                child: ListTile(
                  title: Text('Контакты'),
                  onTap: () {
                    audioPlay();

                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>HelpPage(title: 'Контакты',url: '/contacts',)));

                  },
                  leading: Icon(Icons.contacts,color: Color(0xfffD9E5EA),),
                ),
              ),
              Container(
                height: 60,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: Color(0xffDFE0E3)))
                ),
                child: CheckboxListTile(
                  value: sound,
                  tristate: true,
                  onChanged: (audio)=>setAudio(audio),
                  title: Text('Звук'),
                  secondary: Icon(Icons.audiotrack,color: Color(0xfffD9E5EA)),
                ),
              ),
              Container(
                height: 60,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(top: BorderSide(color: Color(0xffDFE0E3)))
                ),
                child: ListTile(
                  title: Text('Выйти'),
                  onTap: ()async {
                    audioPlay();

                    final prefs = await SharedPreferences.getInstance();
                    prefs.remove('token');
                    pushNewScreen(
                        context,
                        screen: WelcomeScreen(),
                        withNavBar: false,
                        pageTransitionAnimation: PageTransitionAnimation.cupertino
                    );
                  },
                  leading: Icon(Icons.exit_to_app,color: Color(0xfffD9E5EA),),
                ),
              ),


              Container(
                height: h/2-90,
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}