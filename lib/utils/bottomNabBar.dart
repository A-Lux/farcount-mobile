import 'package:flutter/material.dart';

class AppBottomBar extends StatelessWidget {
  final int currentIndex;
  final void Function(int) onTap;

  AppBottomBar({ Key key, this.currentIndex, this.onTap }) : super(key: key);


  Widget build(BuildContext context) {
    return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndex,
        onTap: onTap,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Главное')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_box),
              title: Text('Профиль')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.storage),
              title: Text('Продажи')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.add_shopping_cart),
              title: Text('Склад')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.analytics),
              title: Text('Аналитика')
          ),
        ]
    );
  }
}
