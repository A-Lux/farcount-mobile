import 'dart:io';
import 'dart:typed_data';

import 'package:blue_thermal_printer/blue_thermal_printer.dart';

import 'package:charset_converter/charset_converter.dart';
import 'package:farcount/Printing/PrintingIos.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:native_pdf_renderer/native_pdf_renderer.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as dart_image;
import 'package:pdf/pdf.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:printing/printing.dart';
import 'package:pdf/widgets.dart' as pw;

class BluetoothPage extends StatefulWidget {
  final waybill;
  final products;
  final image;
  const BluetoothPage({Key key, this.waybill, this.products, this.image}) : super(key: key);
  @override
  _BluetoothPageState createState() => _BluetoothPageState();
}

class _BluetoothPageState extends State<BluetoothPage> {

  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  List<pw.TableRow> pdfWidgets = [];

  List<BluetoothDevice> _devices = [];
  BluetoothDevice _device;
  bool _connected = false;
  String pathImage;

  void checkPermission()async{
    var status = await Permission.location.status;
    var newStatus = await Permission.camera.status;

    if (status.isUndetermined) {
      // We didn't ask for permission yet.
      print('sadads');
    }
// You can can also directly ask the permission about its status.
    if (status.isRestricted) {
      // The OS restricts access, for example because of parental controls.
    }

    if(status.isGranted){

    }

    if(status.isDenied){

    }

    if(status.isPermanentlyDenied){

    }
    if (newStatus.isUndetermined) {
      // We didn't ask for permission yet.
      print('asdasdsad');
    }
// You can can also directly ask the permission about its status.
    if (newStatus.isRestricted) {
      // The OS restricts access, for example because of parental controls.
    }

    if(newStatus.isGranted){

    }

    if(newStatus.isDenied){

    }

    if(newStatus.isPermanentlyDenied){

    }
  }

  String tips = 'no device connect';
    static const platform = const MethodChannel('rongta.print/info');

  dynamic rongta;
  Future _loadRongtaPrinterSdkMethod()async{
    try{
      dynamic value = await platform
          .invokeMethod('getPrint');

      return value;
    }on PlatformException catch (e) {
      print(e);


    }

  }

  @override
  void initState() {
    super.initState();

    if(Platform.isAndroid){
      checkPermission();
      initPlatformState();
      initSavetoPath();
    }else{
      _loadRongtaPrinterSdkMethod().then((value){
        setState((){
          rongta = value;
        });
      });

    }
    this.widget.waybill['total_sum'] = this.widget.waybill['total_sum'].toString();
    this.widget.waybill['paid'] = this.widget.waybill['paid'].toString();
    print(this.widget.waybill['total_sum']);
    print(this.widget.products);
  }

  initSavetoPath()async{
    //read and write
    //image max 300px X 300px

  }


  Future<void> initPlatformState() async {
    bool isConnected=await bluetooth.isConnected;
    List<BluetoothDevice> devices = [];
    try {
      devices = await bluetooth.getBondedDevices();
    } on PlatformException {
      // TODO - Error
    }

    bluetooth.onStateChanged().listen((state) {
      switch (state) {
        case BlueThermalPrinter.CONNECTED:
          setState(() {
            _connected = true;
          });
          break;
        case BlueThermalPrinter.DISCONNECTED:
          setState(() {
            _connected = false;
          });
          break;
        default:
          print(state);
          break;
      }
    });

    if (!mounted) return;
    setState(() {
      _devices = devices;
    });

    if(isConnected) {
      setState(() {
        _connected=true;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Распечатать накладную'),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(

            children: <Widget>[

              Container(
                height: 400,
                child: Center(
                  child:PdfPreview(
                    allowPrinting: false,
                    build: (format)async{
                      final doc = pw.Document();


                      final font = await rootBundle.load("assets/OpenSans-Regular.ttf");
                      final ttf = pw.Font.ttf(font);
                      pdfWidgets = [];
                      this.widget.products.forEach((element) {
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Название товара',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['title']}',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Артикул',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['articul']}',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Количество',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['amount'].abs()}',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Цена',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['price'].abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );
                        pdfWidgets.add(
                          pw.TableRow(
                              children: [
                                pw.Text('Общая цена',style: pw.TextStyle(font: ttf,fontSize: 16)),
                                pw.Text('${element['amount'].abs()*element['price']} тг',style: pw.TextStyle(font: ttf,fontSize: 16)),
                              ]
                          ),
                        );

                      });

                      doc.addPage(
                        pw.Page(
                            build: (pw.Context context) =>pw.Column(
                                children: [
                                  pw.Table(
                                      border:pw.TableBorder.all(color: PdfColor.fromHex('#fff')),
                                      children: [
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Накладная ${this.widget.waybill['id']}',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Партнер ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                              pw.Text('${this.widget.waybill['partner']['name']} ',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Общая сумма ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                              pw.Text('${int.parse(this.widget.waybill['total_sum']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),

                                        pw.TableRow(
                                            children: [
                                              pw.Text('Оплачено ',style: pw.TextStyle(font: ttf,fontSize: 18)),
                                              pw.Text('${int.parse(this.widget.waybill['paid']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),


                                            ]
                                        ),
                                        pw.TableRow(
                                            children: [
                                              pw.Text('Осталось ',style: pw.TextStyle(font: ttf,fontSize: 16)),

                                              pw.Text(' ${int.parse(this.widget.waybill['total_sum']).abs()-int.parse(this.widget.waybill['paid']).abs()} тг',style: pw.TextStyle(font: ttf,fontSize: 18)),

                                            ]
                                        ),



                                      ]
                                  ),
                                  pw.Padding(

                                    child: pw.Table(
                                        border:pw.TableBorder.all(color: PdfColor.fromHex('#fff')),
                                        children: pdfWidgets
                                    ),
                                    padding: const pw.EdgeInsets.only(top: 20),
                                  )
                                ]
                            )
                        ),
                      );
                      return doc.save();
                    },


                  ) ,
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 10,),
                  Text(
                    'Устройства:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 30,),
                  Expanded(
                    child: DropdownButton(
                      items: _getDeviceItems(),
                      onChanged: (value) => setState(() => _device = value),
                      value: _device,
                    ),
                  ),
                ],
              ),
              StreamBuilder<List<BluetoothDevice>>(

                initialData: [],
                builder: (c, snapshot) => Column(
                  children: snapshot.data.map((d) => ListTile(
                    title: Text(d.name??''),
                    subtitle: Text(d.address),
                    onTap: () async {
                      setState(() {
                        _device = d;
                      });
                    },
                    trailing: _device!=null && _device.address == d.address?Icon(
                      Icons.check,
                      color: Colors.green,
                    ):null,
                  )).toList(),
                ),
              ),
              SizedBox(height: 10,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  RaisedButton(
                    color: Theme.of(context).primaryColor,
                    onPressed:(){
                      initPlatformState();

                    },
                    child: Text('Обновить', style: TextStyle(color: Colors.white),),
                  ),
                  SizedBox(width: 20,),
                  RaisedButton(
                    color: _connected ?Colors.red:Colors.green,
                    onPressed:
                    _connected ? _disconnect : _connect,
                    child: Text(_connected ? 'Отключится' : 'Подключится', style: TextStyle(color: Colors.white),),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 50),
                child:  RaisedButton(
                  color: Theme.of(context).primaryColor,
                  onPressed: (){
                    _tesPrint();
                  },

                  child: Text('Распечатать', style: TextStyle(color: Colors.white)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


  List<DropdownMenuItem<BluetoothDevice>> _getDeviceItems() {
    List<DropdownMenuItem<BluetoothDevice>> items = [];
    if (_devices.isEmpty) {
      items.add(DropdownMenuItem(
        child: Text('NONE'),
      ));
    } else {
      _devices.forEach((device) {
        items.add(DropdownMenuItem(
          child: Text(device.name),
          value: device,
        ));
      });
    }
    return items;
  }
  void _tesPrint() async {
    //SIZE
    // 0- normal size text
    // 1- only bold text
    // 2- bold with medium text
    // 3- bold with large text
    //ALIGN
    // 0- ESC_ALIGN_LEFT
    // 1- ESC_ALIGN_CENTER
    // 2- ESC_ALIGN_RIGHT
    bluetooth.isConnected.then((isConnected) {
      if (isConnected) {

        bluetooth.printNewLine();

        bluetooth.printLeftRight("Накладная ${this.widget.waybill['id']}", "",3,charset: 'cp866',);
        bluetooth.printLeftRight("Партнер", "${this.widget.waybill['partner']['name']}",3,charset: 'cp866',);
        bluetooth.printLeftRight("Сумма", "${int.parse(this.widget.waybill['total_sum']).abs()} тг",3,charset: 'cp866',);
        bluetooth.printLeftRight("Оплачено", "${int.parse(this.widget.waybill['paid']).abs()} тг",3,charset: 'cp866',);
        bluetooth.printLeftRight("Осталось", "${int.parse(this.widget.waybill['total_sum']).abs()-int.parse(this.widget.waybill['paid']).abs()} тг",3,charset: 'cp866',);

        this.widget.products.forEach((product){
          bluetooth.printNewLine();

          bluetooth.printLeftRight("Название товара", "${product['title']}",1,charset: 'cp866',);
          bluetooth.printLeftRight("Артикул", "${product['articul']}",1,charset: 'cp866',);
          bluetooth.printLeftRight("Количество", "${product['amount'].abs()}",1,charset: 'cp866',);
          bluetooth.printLeftRight("Цена", "${product['price']} тг",1,charset: 'cp866',);
          bluetooth.printLeftRight("Общая цена", "${product['price']*product['amount'].abs()} тг",1,charset: 'cp866',);


        });
        bluetooth.paperCut();
      }
    });
  }


  void _connect() {
    if (_device == null) {
      show('No device selected.');
    } else {
      bluetooth.isConnected.then((isConnected) {
        if (!isConnected) {
          bluetooth.connect(_device).catchError((error) {
            setState(() => _connected = false);
          });
          setState(() => _connected = true);
        }
      });
    }
  }


  void _disconnect() {
    setState(() => _connected = true);
    bluetooth.disconnect();

  }

//write to app path
  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  Future show(
      String message, {
        Duration duration: const Duration(seconds: 3),
      }) async {
    await new Future.delayed(new Duration(milliseconds: 100));
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text(
          message,
          style: new TextStyle(
            color: Colors.white,
          ),
        ),
        duration: duration,
      ),
    );
  }


   bluetoothInit() async{


  }



















//write to app path


}
