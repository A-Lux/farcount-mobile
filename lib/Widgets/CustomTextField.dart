import 'package:flutter/material.dart';
class CustomTextField extends StatelessWidget {
  final hintText;
  final prefixIcon;
  final onChanged;
  final obscure;
  final textCtr;
  final headText;
  final hasExtra;
  final acionText;
  final onTapExtra;
  final formatter;
  final keyboard;

  CustomTextField({this.hintText, this.prefixIcon, this.obscure,this.onChanged,this.textCtr,this.headText,this.hasExtra,this.acionText,this.onTapExtra, this.formatter, this.keyboard});
  @override
  Widget build(BuildContext context) {
    var c = Theme.of(context);
    var w = MediaQuery.of(context).size.width;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    headText,
                    style: TextStyle(
                        color: Color(0xff181E28),
                        fontSize: 14,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                 if (hasExtra ?? false) GestureDetector(
                   onTap: onTapExtra,
                   child: Text(
                      acionText,
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 14,
                          fontWeight: FontWeight.w600
                      ),
                    ),
                 ),
                ],
              ),
            ),
            SizedBox(height: 8,),
            Container(
              width: w -68,
              height: 70,
              child: TextField(
                controller: textCtr,
                cursorColor: Colors.black,
                inputFormatters: this.formatter != null ? [this.formatter] : null,
                obscureText: obscure ?? false,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  hintText: hintText,
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                    borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                  ),
                ),
                keyboardType:this.keyboard == null ?  TextInputType.emailAddress : this.keyboard,
                style: new TextStyle(
                  color: Colors.black,
                ),
                onChanged: onChanged,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
