import 'package:flutter/material.dart';
class BlueButton extends StatelessWidget {
  final text;
  final VoidCallback onTap;
  BlueButton({this.text,this.onTap});
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: w-32,
        height: 70,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Theme.of(context).primaryColor
        ),
        child: Center(
          child:text != '' ? Text(text,style: TextStyle(
              fontSize: 18,
              color: Colors.white
          ),) : CircularProgressIndicator(
            backgroundColor: Colors.white,
          ),
        ),
      ),
    );
  }
}
