import 'package:flutter/material.dart';
class Heading extends StatelessWidget {
  final text;
  Heading(this.text);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 24,
      left: 16,
      child: Container(
        width: MediaQuery.of(context).size.width*0.8,
        child: Text(text,style: TextStyle(
          color: Colors.black,
          fontSize: 30
        ),),
      ),
    );
  }
}
