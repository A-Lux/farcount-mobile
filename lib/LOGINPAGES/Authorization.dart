import 'dart:convert';

import 'package:farcount/AppViewController.dart';
import 'package:farcount/LOGINPAGES/ForgetPasswordPage.dart';
import 'package:farcount/LOGINPAGES/RegistrationScreen.dart';
import 'package:farcount/Pages/MainPage.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:farcount/utils/bottomNabBar.dart';
import 'package:farcount/Widgets/BlueActionButton.dart';
import 'package:farcount/Widgets/CustomTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:http/http.dart' as http;
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';

import '../Setting.dart';
class AuthorizationScreen extends StatefulWidget {
  static var id = 'authorization';

  @override


  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  var maskFormatter = new MaskTextInputFormatter(mask: '+# (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });
  TextEditingController _phoneController = new MaskedTextController(mask: '+0 (000) 000-00-00', text: '+7');

  final _formMain = GlobalKey<FormState>();
  bool loading = false;
  String _phone;
  TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();



  }

  @override




  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Stack(
            alignment: Alignment.center,
            children: [

              Container(
                width: w  ,
                child: Form(
                  key: _formMain,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Image(image: AssetImage('assets/logo.png',),width: 200,),
                      ),
                      Text(
                        'Авторизация',
                        style: TextStyle(
                          color: Color(0xff0E1017),
                          fontSize: 24,
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 32),
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 16),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Ваш номер телефона',
                                      style: TextStyle(
                                          color: Color(0xff181E28),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              SizedBox(height: 8,),
                              Container(
                                width: w -68,
                                height: 70,
                                child: TextField(
                                  controller: _phoneController,
                                  cursorColor: Colors.black,
                                  obscureText:  false,
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                      borderRadius: new BorderRadius.circular(8.0),
                                    ),
                                    hintText: '+7',

                                    border: new OutlineInputBorder(
                                      borderRadius: new BorderRadius.circular(8.0),
                                      borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                                    ),
                                  ),
                                  keyboardType: TextInputType.phone,
                                  style: new TextStyle(
                                    color: Colors.black,
                                  ),
                                  onChanged: (value)=>this._phone = value,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CustomTextField(

                        headText: 'Ваш пароль',
                        hasExtra: true,
                        obscure: true,
                        acionText: 'Забыли пароль?',
                        hintText: '',
                        textCtr: _passwordController,
                        onTapExtra: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>ForgetPasswordPage()));
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 34),
                          child: BlueButton(
                            text: loading == true ? '' : 'Войти',
                            onTap: ()async{
                              audioPlay();

                              List<String> errors = [];
                            if(_phoneController.text == ''){
                              errors.add('Введите номер телефона');
                            }
                            
                            if(_passwordController.text == ''){
                              errors.add('Введите пароль');
                            }

                            if(errors.length > 0){
                              SweetAlert.show(
                                context,
                                title: 'Ошибка',
                                subtitle: errors.join('\n'),
                                style: SweetAlertStyle.error
                              );
                              return true;
                            }


                            final endpoint = endpointApi;

                            try{
                              setState(() {
                                loading = true;
                              });

                              print(_phoneController.text);
                              final response =await http.post('$endpoint/sanctum/token',body:{
                                "phone" : this._phoneController.text,
                                "password" : this._passwordController.text,
                                "device_name" : "dwdwd"
                              });
                              print(response.body);

                              if(response.statusCode == 200){
                                final data = jsonDecode(response.body);
                                print(data);
                                final prefs = await SharedPreferences.getInstance();
                                prefs.setString('token', data['token']);
                                print(data['token']);

                                setState(() {
                                  loading = false;
                                });
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(builder: (context) => AppViewController()),
                                      (Route<dynamic> route) => false,
                                );
                              }else{
                                print(response.body);
                                setState(() {
                                  loading = false;
                                });
                                SweetAlert.show(context,
                                  title: 'Ошибка',
                                  subtitle: 'Не правильный пароль или логин',
                                  style: SweetAlertStyle.error
                                );
                              }

                            }catch(e){
                              print(e);
                            }

                          },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
//            Positioned(
//                bottom: h*0.05,
//                child: Column(
//                  children: [
//                    Text(
//                      'Еще нет аккаунта?',
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontSize: 18,
//                      ),
//                    ),
//                    SizedBox(
//                      height: 8,
//                    ),
//                    GestureDetector(
//                      onTap: (){
//                        Navigator.pushNamed(context, RegistrationScreen.id);
//                      },
//                      child: Text(
//                        'Регистрация',
//                        style: TextStyle(
//                          color: Theme.of(context).primaryColor,
//                          fontSize: 18,
//                        ),
//                      ),
//                    ),
//
//                  ],
//                )
//            )
            ],
          ),
        ),
      ),
    );
  }
}