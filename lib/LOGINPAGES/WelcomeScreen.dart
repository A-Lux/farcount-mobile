import 'dart:io';

import 'package:audioplayers/audio_cache.dart';
import 'package:farcount/AppViewController.dart';
import 'package:farcount/LOGINPAGES/Authorization.dart';
import 'package:farcount/Setting.dart';
import 'package:farcount/Widgets/BlueActionButton.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  AudioCache audioCache = AudioCache();

  void setAudio()async{
    final prefs = await SharedPreferences.getInstance();
    final audio = prefs.getBool('audio');
    if(audio == null){
      prefs.setBool('audio', true);
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    this.getRedirect();
//    if (Platform.isIOS) {
//      if (audioCache.fixedPlayer != null) {
//        audioCache.fixedPlayer.startHeadlessService();
//      }
//    }

  }
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 0,
              child: Image(image: AssetImage('assets/logo.png',),width: 200,),
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Авторизация',
                    style: TextStyle(
                      color: Color(0xff0E1017),
                      fontSize: 24,
                    ),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 34),
                    child: BlueButton(text: 'Продавец',onTap: (){
                      audioPlay();
                      Navigator.push(context,MaterialPageRoute(builder: (context)=>AuthorizationScreen()));
                    },),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: (){
                      audioPlay();

                      Navigator.push(context,MaterialPageRoute(builder: (context)=>AuthorizationScreen()));
                    },
                    child: Container(
                      width: w-68,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: Theme.of(context).primaryColor)
                      ),
                      child: Center(
                        child: Text('Хозяин',style: TextStyle(
                          fontSize: 18,
                          color: Color(0xff0E1017)
                        ),),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: h*0.05,
              child: Column(
                children: [
                  Text(
                    'Создавайте накладные',
                    style: TextStyle(
                      color: Color(0xffC2CACD),
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    'легко за 2 минуты',
                    style: TextStyle(
                      color: Color(0xffC2CACD),
                      fontSize: 18,
                    ),
                  ),
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}
