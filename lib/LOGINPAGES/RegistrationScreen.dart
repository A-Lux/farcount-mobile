import 'package:farcount/Widgets/BlueActionButton.dart';
import 'package:farcount/Widgets/CustomTextField.dart';
import 'package:flutter/material.dart';
class RegistrationScreen extends StatelessWidget {
  static var id = 'registrationScreen';
  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image(image: AssetImage('assets/logo.png',),width: 200,),
              Text(
                'Регистрация',
                style: TextStyle(
                  color: Color(0xff0E1017),
                  fontSize: 24,
                ),
              ),
              SizedBox(
                height: 35,
              ),
              CustomTextField(
                headText: 'Ваш номер телефона',
                hintText: '+7',
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                headText: 'Ваш пароль',
                hintText: '',
                obscure: true,
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                headText: 'Повторите пароль',
                hintText: '',
                obscure: true,
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 34),
                child: BlueButton(text: 'Регистрация',),
              ),
              SizedBox(height: 30,),
              Text(
                'Уже есть аккаунт? ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                ),
              ),
              SizedBox(
                height: 8,
              ),
              GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Text(
                  'Авторизация',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(height: 100,),
            ],
          ),
        ),
      ),
    );
  }
}