import 'dart:convert';

import 'package:farcount/LOGINPAGES/Authorization.dart';
import 'package:farcount/Widgets/BlueActionButton.dart';
import 'package:farcount/Widgets/CustomTextField.dart';
import 'package:farcount/provider/AuthProvider.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:http/http.dart' as http;

import '../Setting.dart';

class ForgetPasswordPage extends StatefulWidget {
  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  var maskFormatter = new MaskTextInputFormatter(mask: '+# (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.of(context).size.width;
    var h = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Stack(
            alignment: Alignment.center,
            children: [

              Container(
                width: w  ,
                child: Form(

                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Image(image: AssetImage('assets/logo.png',),width: 200,),
                      ),
                      Text(
                        'Введите номер телефона чтобы восстановить пароль',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff0E1017),
                          fontSize: 24,
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 32),
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 16),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Ваш номер телефона',
                                      style: TextStyle(
                                          color: Color(0xff181E28),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                              SizedBox(height: 8,),
                              Container(
                                width: w -68,
                                height: 70,
                                child: TextField(
                                  inputFormatters: [maskFormatter],
                                  controller: _phoneController,
                                  cursorColor: Colors.black,
                                  obscureText:  false,
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black),
                                      borderRadius: new BorderRadius.circular(8.0),
                                    ),
                                    hintText: '+7',

                                    border: new OutlineInputBorder(
                                      borderRadius: new BorderRadius.circular(8.0),
                                      borderSide: new BorderSide(color:   Color(0xffDFE0E3)),
                                    ),
                                  ),
                                  keyboardType: TextInputType.phone,
                                  style: new TextStyle(
                                    color: Colors.black,
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 34),
                          child: BlueButton(
                            text: loading == true ? '' : 'Войти',
                            onTap: ()async{
                              audioPlay();

                              List<String> errors = [];
                              if(_phoneController.text == ''){
                                errors.add('Введите номер телефона');
                              }



                              if(errors.length > 0){
                                SweetAlert.show(
                                    context,
                                    title: 'Ошибка',
                                    subtitle: errors.join('\n'),
                                    style: SweetAlertStyle.error
                                );
                                return true;
                              }


                              final endpoint = endpointApi;

                              try{
                                setState(() {
                                  loading = true;
                                });

                                final response =await http.post('$endpoint/sanctum/forget',body:{
                                  "phone" : this._phoneController.text,
                                  "device_name" : "dwdwd"
                                });
                                final data = jsonDecode(response.body);

                                if(response.statusCode == 200){
                                  print(data);
                                  final prefs = await SharedPreferences.getInstance();
                                  prefs.setString('token', data['token']);
                                  print(data['token']);

                                  setState(() {
                                    loading = false;
                                  });
                                  SweetAlert.show(context,
                                      title: 'Успешно',
                                      subtitle: data['message'],
                                      style: SweetAlertStyle.success,
                                      onPress: (bool isConfirm){
                                        Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(builder: (context) => AuthorizationScreen()),
                                              (Route<dynamic> route) => false,
                                        );
                                        return false;
                                      }
                                  );

                                }else{
                                  setState(() {
                                    loading = false;
                                  });
                                  SweetAlert.show(context,
                                      title: 'Ошибка',
                                      subtitle: data['message'],
                                      style: SweetAlertStyle.error
                                  );
                                }

                              }catch(e){
                                print(e);
                              }

                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
//            Positioned(
//                bottom: h*0.05,
//                child: Column(
//                  children: [
//                    Text(
//                      'Еще нет аккаунта?',
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontSize: 18,
//                      ),
//                    ),
//                    SizedBox(
//                      height: 8,
//                    ),
//                    GestureDetector(
//                      onTap: (){
//                        Navigator.pushNamed(context, RegistrationScreen.id);
//                      },
//                      child: Text(
//                        'Регистрация',
//                        style: TextStyle(
//                          color: Theme.of(context).primaryColor,
//                          fontSize: 18,
//                        ),
//                      ),
//                    ),
//
//                  ],
//                )
//            )
            ],
          ),
        ),
      ),
    );
  }
}
