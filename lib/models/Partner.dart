
class PartnerData{
  Map<String,dynamic> data;

  PartnerData({this.data});


  PartnerData.fromJson(Map<String,dynamic> json){
    data: Partners.fromJon(json['data']);
  }

}


class Partners {
  List<dynamic> partners;


  Partners({this.partners});

  Partners.fromJon(Map<String,dynamic> json){
    json.forEach((key, value) {
      partners.add(value);


    });


  }
}


class Partner{
  int id;
  String name;
  String phone;

  Partner(this.id, this.name, this.phone);


  Partner.fromJson(Map<String,dynamic> json){
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
  }

}